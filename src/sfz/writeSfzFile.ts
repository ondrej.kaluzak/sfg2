import { SFG } from '../types/endpoints';
import { writeWaveFile } from '../wavefiles';
import fs from 'fs';
import { BR, SR } from '../utils/envConsts';

const SFZ_HEADER = '<group>\nlovel=0\nhivel=127\n';

// ak je 1 tak je to full panning do stran, cim vacsie tim je uzsie, asi ma zmysel davat hodnoty max 10;
const MONO_AMMOUNT = 1.5;

export const writeSfz = (
    path: string,
    name: string,
    waves: SFG.Wave[],
    offset: number,
    loopSettings: {
        loop: boolean;
        releaseTime: number;
        loopIndexes: { lStart: number; lEnd: number }[];
    }
) => {
    let sfz = SFZ_HEADER;
    const { loop, releaseTime, loopIndexes } = loopSettings;
    const numberOfTones = waves.length;
    const centerPanIndex = numberOfTones / 2;
    const mono = centerPanIndex * MONO_AMMOUNT;

    const sfzRow = (index: number, releaseTime: number, pan: number) =>
        `<region> trigger=attack pan=${
            pan * 100
        } pitch_keycenter=${index} lokey=${index} hikey=${index} ampeg_release=${
            releaseTime < 0.001 ? 0.01 : releaseTime
        } sample=samples/note_${index}.wav\n`;
    const sfzRowLoop = (
        index: number,
        ls: number,
        le: number,
        releaseTime: number,
        pan: number
    ) =>
        `<region> trigger=attack  pan=${
            pan * 100
        } loop_mode=loop_sustain loop_start=${ls} loop_end=${le} pitch_keycenter=${index} lokey=${index} hikey=${index} ampeg_release=${
            releaseTime < 0.001 ? 0.01 : releaseTime
        } sample=samples/note_${index}.wav\n`;

    const sfzPath = `${path}/sfz`;
    if (!fs.existsSync(sfzPath)) {
        fs.mkdirSync(sfzPath);
    }
    const samplesPath = `${sfzPath}/samples`;
    if (!fs.existsSync(samplesPath)) {
        fs.mkdirSync(samplesPath);
    }

    waves.map((w, i) => {
        const pan = (i - centerPanIndex + 0.5) / mono;
        if (loop) {
            sfz =
                sfz +
                sfzRowLoop(
                    i + offset,
                    loopIndexes[i].lStart,
                    loopIndexes[i].lEnd,
                    releaseTime,
                    pan
                );
        } else {
            sfz = sfz + sfzRow(i + offset, releaseTime, pan);
        }

        writeWaveFile(`${samplesPath}/note_${i + offset}.wav`, 1, SR, BR, w);
    });
    fs.writeFileSync(`${sfzPath}/${name}.sfz`, sfz);
};
