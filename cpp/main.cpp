#include <iostream>
#include <math.h>
#include "AudioFile.h"
using namespace std;

typedef int Coma[2];

constexpr double SR = 44100;
constexpr double PI = 6.283185307179586476925286766559;
#define LOG(x) std::cout << x << std::endl

#define _USE_MATH_DEFINES

//#define PI2 = m

void printArray(double a[], int size)
{

    for (int i = 0; i < size; i++)
    {
        std::cout << a[i] << '\t';
    }
}

int *parse()
{
    int *c = new Coma;
    c[0] = 10;
    c[1] = 20;
    c[2] = 30;
    c[3] = 40;

    return c;
}

double *genWave(float freq, float ms, float amp, int &size)
{

    const int count = floor(SR * (ms / 1000));
    const double period = SR / freq;
    double *wave = new double[count];
    for (int i = 0; i < count; i++)
    {
        wave[i] = sin((i * PI) / period) * amp;
    }
    size = count; // * sizeof(double);

    return wave;
}

double *genExpSweep(double f1, double f2, double ms, double amp, int& size){
     int count = floor(SR * (ms / 1000));
     
     double step = pow((double)f2/f1, (double)1/(double)count);
     LOG(step);
     LOG(count);
    double *wave = new double[count];
    double f = f1;
    for (int i = 0; i < count; i++)
    {   double period = (double) SR / f;
        wave[i] = sin((i *  PI) / period) * amp;
        LOG(((i *  PI) / period) * amp);
        f = f * step;
    }
    size = count;
    return wave;
}

double *genLinSweep(double f1, double f2, double ms, double amp, int& size){
     int count = floor(SR * (ms / 1000));
     
     double step = (f1-f2)/(double)count;
     
    double *wave = new double[count];
    
    for (int i = 0; i < count; i++)
    {   double period = (double) SR / (f1+i*step);
        wave[i] = sin((static_cast<float> (i) *  PI) / period) * amp;
        //LOG(((i *  PI) / period) * amp);
        
    }
    size = count;
    return wave;
}

int main()
{
    
    //const float sampleRate = 44100.f;

    int size = 0;
    double *wave = genWave(15000, 15000, 1, size);
    //double *wave = genLinSweep(30,15000, 1000, 1, size);
    LOG("SIZE:");
    LOG(size);
    LOG("VALS:");
//    return 0;


    AudioFile<double> a;
    a.setNumChannels(1);
    a.setSampleRate((int)SR);
    a.setNumSamplesPerChannel(size);

    for (int i = 0; i < a.getNumSamplesPerChannel(); i++)
    {
        for (int channel = 0; channel < a.getNumChannels(); channel++)
        {
            a.samples[channel][i] = wave[i];
        }
    }

    //---------------------------------------------------------------
    // 4. Save the AudioFile

    std::string filePath = "sine-wave2.wav"; // change this to somewhere useful for you
    a.save("sine-wave2.wav", AudioFileFormat::Wave);

    //printArray(wave, size);
    //printArray(a, size);

    return 0;
}