import { SFG } from '../types/endpoints';
import { MAX_ALLOWED_PARTIALS, OUTPUT_DIR } from '../utils/envConsts';
import fs from 'fs';
import { factorToDb } from '../utils/utils';

const MAX_POW = 20;
const MAX_PARTIALS = 32;

export const getPartialsTable = (
    primes: SFG.Vector,
    maxPartials?: number,
    brightness?: number
): SFG.Partials => {
    let MP = maxPartials ?? MAX_PARTIALS;
    if (MP > MAX_ALLOWED_PARTIALS) MP = MAX_ALLOWED_PARTIALS;
    const brt = brightness ?? 1;
    const factorsAll: SFG.PartialFactor[] = [];
    const realPartialIndex: number[] = [];
    for (var x = 0; x < MAX_POW; x++) {
        for (var y = 0; y < MAX_POW; y++) {
            for (var z = 0; z < MAX_POW; z++) {
                factorsAll.push(
                    Math.pow(primes[0], x) *
                        Math.pow(primes[1], y) *
                        Math.pow(primes[2], z)
                );
                realPartialIndex.push(2 ** x * 3 ** y * 5 ** z);
            }
        }
    }

    const factors = factorsAll.sort((a, b) => a - b).slice(0, MP);
    const realSorted = realPartialIndex.sort((a, b) => a - b).slice(0, MP);
    //console.log(realSorted);
    const amps = [...Array(MP).keys()].map((i) => {
        if (singleSide.find((a) => a === i)) {
            return (1 / factors[i] ** brt) * 0.5;
        } else if (doubleSide.find((a) => a === i) || i > 30) {
            return (1 / factors[i] ** brt) * 0.5;
        } else return 1 / factors[i] ** brt;
    });
    let str = '';
    factors.forEach(
        (f, i) =>
            (str += `${realSorted[i]}.harmonic\t factor: ${f.toFixed(
                10
            )}\t amp: ${amps[i].toFixed(10)}\t dB: ${factorToDb(
                amps[i]
            ).toFixed(10)}\r\n`)
    );

    console.log(
        `\nWriting partials log file to ${OUTPUT_DIR}/partials_log.txt ... \n`
    );
    fs.writeFileSync(`${OUTPUT_DIR}/partials_log.txt`, str);

    return { factors, amps };
};
const ATT = [5, 9, 13, 18];

const singleSide = [5, 6, 8, 10, 11, 14, 15, 29, 30];
const doubleSide = [
    9, 12, 13, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 31, 32,
];
//jedna strana:
//5, 6, 8,
