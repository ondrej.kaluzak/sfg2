fs = require('fs');

const wl = 128;
const pIndex = wl / 1;
const wide = pIndex * 1.1;

// midi file pan
for (var i = 0; i < wl; i++) {
    console.log((pIndex - i + 0.5) / wide);
}
// sfz pan
for (var i = 0; i < wl; i++) {
    //console.log((i - pIndex + 0.5) / wide);
}













process.exit();
/************************************************* */
const factorPerSec = 4;
const SR = 44100;
const PI2 = Math.PI * 2;
const oneStep = Math.pow(factorPerSec, 1 / SR);

const dbSpeedPerSec = 6;
const dbSusPoint = -12;
const dbSpeedPerSampel = dbSpeedPerSec / SR;
const finalLengthMs = Math.abs(-12 / dbSpeedPerSampel);
console.log(-12 / dbSpeedPerSampel);
process.exit(0);

const dbToFactor = (db) => Math.pow(10, db / 20);
const factorToDb = (f) => 20 * Math.log10(f);
console.log(factorToDb(0));
return;

const comonFreq = 100;
const baseF = 10;
for (var i = 0; i < 30; i++) {
    const freq = baseF + i * baseF;
    console.log();
    const factor = freq / comonFreq;

    console.log('freq: ', freq, '\t', 'Att: ', 3 * factor);
}

const raisedSin = (ms) => {
    const count = Math.floor((SR * ms) / 1000);
    var out = [];
    for (var i = 0; i < count; i++) {
        out.push(Math.sin((i * PI2) / (count * 4)) ** 2);
    }
    fs.writeFileSync('./output.json', JSON.stringify({ data: out }));
};

raisedSin(5000);
return;
/************************************************* */
//return;
const fadeOutSpeed = (dbPerSec, susPoint, lengthMs) => {
    const oneStep = dbPerSec / SR;
    const count = Math.floor((lengthMs * SR) / 1000);
    const susPointFactor = dbToFactor(susPoint);

    const out = [];
    let amp = 0;
    let i = 0;
    while (i < count) {
        if (amp <= susPoint) {
            out.push(susPointFactor);
            i++;
            continue;
        }
        out.push(dbToFactor(amp));
        amp -= oneStep;
        i++;
    }
    console.log(factorToDb(out[out.length - 1]));

    /*  console.log(
         `${i} iterations, ${factor} breaking factor, one step: ${oneStep}\n\n`
     ); */

    fs.writeFileSync('./output.json', JSON.stringify({ data: out }));
    //return normalize(out);
};

fadeOutSpeed(8, -16, 2012);

const makeTone = (freq, partials, envMs, envLength) =>
    sumWaves(
        [...Array(partials.factors.length)].map((_, i) => {
            if (
                partials.amps[i] < 0.00001 ||
                freq * partials.factors[i] > 18000
            ) {
                return gen1SMP(0);
            }
            const partialFreq = freq * partials.factors[i];
            const commonFreq = 100;
            const w = genWave(partialFreq, envLength, partials.amps[i]);

            const dbPerSec = partialFreq / commonFreq;
            const targetPointDb = 0.1;
            //console.log(dbPerSec);
            //console.log(targetPointDb);

            const decayFo = fadeOutSpeed(dbPerSec, targetPointDb);
            // console.log();
            // console.log();
            console.log(decayFo.length);
            // console.log();
            // console.log();
            //return decayFo;
            return amWith(w, decayFo, envMs.attack);
            //return amWaves([FO, w]);
        })
    );

/**------------------=====X====X====X======-------------------- */
/**------------------=====X====X====X======-------------------- */
/**------------------=====X====X====X======-------------------- */
/**------------------=====X====X====X======-------------------- */
return;

console.log(oneStep);
var wave = [];
for (var i = 0; i < SR; i++) {
    wave.push(Math.pow(oneStep, i));
}
console.log(Math.pow(oneStep, 500));

fs.writeFileSync('./output.json', JSON.stringify({ data: wave }));

return;

const interpolate = (x, y, steps) => {
    let temp = new Array(steps);
    steps--;
    const onestep = (y - x) / steps;
    temp[0] = x;
    steps--;
    while (steps >= 0) {
        temp[temp.length - steps - 1] = temp[temp.length - steps - 2] + onestep;
        steps--;
    }
    return temp;
};

console.log(interpolate(1, -100, 100));

process.exit(0);

for (var i = 0; i < 100; i++) {
    var x = Math.random() * 2 - 1;
    var n = i % 3 !== 0 ? '\t' : '\n';
    process.stdout.write(x + n);
}

process.exit(0);

const amWaves = (waves) => {
    const longest = Math.max(...waves.map((w) => w.length));
    const amWave = [];
    for (var i = 0; i < longest; i++) {
        amWave.push(
            waves.reduce((acc, w) => acc * (w[i] === undefined ? 1 : w[i]), 1)
        );
    }

    return amWave;
};

function noteToFreq(note) {
    let a = 440; //frequency of A (coomon value is 440Hz)
    return (a / 32) * 2 ** ((note - 9) / 12);
}

/*****************
 * CREATE MIDI MAP
 **************** */

/* const x = new Array(128)
    .fill(0)
    .map((_, i) => i)
    .join(' | ');
console.log(x);
 */
// function getAllFactorsFor(remainder) {
//     var factors = [], i;

//     for (i = 2; i <= remainder; i++) {
//         while ((remainder % i) === 0) {
//             factors.push(i);
//             remainder /= i;
//         }
//     }

//     return factors;
// }

function gcd(arr) {
    // Use spread syntax to get minimum of array

    arr = arr.map((a) => Math.abs(a));
    const lowest = Math.min(...arr);
    const num = arr.length;

    for (let factor = lowest; factor > 1; factor--) {
        let isCommonDivisor = true;

        for (let j = 0; j < num; j++) {
            if (arr[j] % factor !== 0) {
                isCommonDivisor = false;
                break;
            }
        }

        if (isCommonDivisor) {
            return factor;
        }
    }

    return 1;
}

const factorToCents = (f) => 1200 * Math.log2(f);
const centsToFactor = (c) => Math.pow(2, (1 / 1200) * c);
const Oktava = factorToCents(2);
const Duodecima = factorToCents(3);
const Septemdecima = factorToCents(5);

const moveByVector = (v) => {
    return v[0] * okt2 + v[1] * duo2 + v[2] * septem2;
};

const moveByVectorFactor = (v) => {
    return Math.pow(2, v[0]) * Math.pow(3, v[1]) * Math.pow(5, v[2]);
};

const divideVector = (v, k) => {
    let kk = Math.abs(k);
    if (kk === 0) kk = 1;

    return [v[0] / kk, v[1] / kk, v[2] / kk];
};

const getFactors = (coma, vector) => {
    let okt2, duo2, septem2;
    let uncomplete = false;
    if (vector[0] === 0) {
        okt2 = Oktava;
        uncomplete = true;
    } else {
        let sign1 = -1 * (vector[0] / Math.abs(vector[0]));

        okt2 =
            Oktava +
            sign1 *
            ((factorToCents(coma[0] / coma[1]) * Math.log2(2)) /
                Math.log2(coma[0] * coma[1]));
    }
    if (vector[1] === 0) {
        duo2 = Duodecima;
        uncomplete = true;
    } else {
        let sign2 = -1 * (vector[1] / Math.abs(vector[1]));
        duo2 =
            Duodecima +
            sign2 *
            ((factorToCents(coma[0] / coma[1]) * Math.log2(3)) /
                Math.log2(coma[0] * coma[1]));
    }
    if (vector[2] === 0) {
        septem2 = Septemdecima;
        uncomplete = true;
    } else {
        let sign3 = -1 * (vector[2] / Math.abs(vector[2]));

        septem2 =
            Septemdecima +
            sign3 *
            ((factorToCents(coma[0] / coma[1]) * Math.log2(5)) /
                Math.log2(coma[0] * coma[1]));
    }

    return [okt2, duo2, septem2, uncomplete];
};

const f = () => {
    // const coma = [262144, 253125];

    // console.log(Oktava, Duodecima, Septemdecima);

    //  const vector = [5, -9, 4]; // gen = 176,4766 centů => 309.590412*9 - 315.641287

    /******Vishnu **** */
    // const vector = [23, 6, -14];
    // const coma = [6115295232, 6103515625];

    /******** */
    // const vector = [11, -4, -2];
    // const coma = [2048, 2025];

    /**** Magic ****/
    //const vector = [-10, -1, 5];
    //const coma = [3125, 3072];

    /**** hanson** */
    //const vector = [-6, -5, 6];
    //const coma = [15625, 15552];

    /**** Mavila ** */
    //const vector = [-7, 3, 1];
    //const coma = [135, 128];

    /**** Meantone ** */
    //const vector = [-4, 4, 1];
    //const coma = [81, 80];

    /**** Porcupine ** */
    //const vector = [1, -5, 3];
    //const coma = [250, 243];

    /**** Dimipent ** */
    const vector = [3, 4, -4];
    const coma = [648, 625];

    /**** Tetracot ** */
    // const vector = [5, -9, 4];
    // const coma = [20000, 19683];

    /**** Augemnter ** */
    //const vector = [7, 0, -3];
    //const coma = [128, 125];

    /**** Blackwood ** */
    //const vector = [8, -5, 0];
    //const coma = [256, 243];

    /**** Compton ** */
    // const vector = [0, 3, -2];
    // const coma = [27, 25];

    /**** Father ** */
    // const vector = [-4, 1, 1];
    // const coma = [15, 16];

    /**** Bug ** */
    // const vector = [-19, 12, 0];
    // const coma = [531441, 524288];

    console.log('Input vector, ', vector);
    const [okt3, duo2, septem2, uncomplete] = getFactors(coma, vector);
    console.log('Input vector, ', vector);
    const gcdAll = gcd(vector);
    const gcdRest = gcd([vector[1], vector[2]]);
    console.log('GCD', gcdAll, gcdRest);

    const fac = gcdRest / gcdAll;
    okt2 = okt3 / fac;
    console.log([Oktava, Duodecima, Septemdecima]);
    console.log([okt2, duo2, septem2]);

    //Ak je aspon jedna suradnica vektoru 0;
    if (uncomplete) {
        const v = vector;
        if (v[0] === 0) {
            period = Oktava;
            const amount = Math.abs(v[2]);
            const gen = Math.abs((duo2 - amount * period) / amount);
            console.log(period, gen, period - gen);
        }

        if (v[1] === 0) {
            period = Math.abs(okt2 / v[2]);
            const maxPeriod = Math.floor(Duodecima / period);
            const gen = Duodecima - period * maxPeriod;
            console.log(period, gen, period - gen);
        }
        if (v[2] === 0) {
            const period = Math.abs(okt2 / v[1]);
            const maxPeriod = Math.floor(septem2 / period);
            const gen = Septemdecima - period * maxPeriod;
            console.log(period, gen, period - gen);
        }

        return;
    }

    const moveByVector = (v) => {
        return v[0] * okt2 + v[1] * duo2 + v[2] * septem2;
    };

    const v1 = [0, vector[1] / fac, 0];
    const v2 = [0, 0, vector[2] / fac];
    console.log(v1);
    console.log(v2);

    const p0 = vector[0];
    const p1 = vector[1] / fac;
    const p2 = vector[2] / fac;

    const all = new Array();

    const MAX_PROLONG = 3;

    for (let prolong = 1; prolong < MAX_PROLONG; prolong++) {
        for (
            let i = -1 * Math.abs(p2) * prolong;
            i <= Math.abs(p2) * prolong;
            i++
        ) {
            for (
                let j = -1 * Math.abs(p1) * prolong;
                j <= Math.abs(p1) * prolong;
                j++
            ) {
                if (p1 * i + p2 * j === p0) {
                    console.log('\n\n*********************');
                    console.log('\nriesenie: ', i, j);
                    v1[0] = v1[1] * i;
                    v2[0] = v2[2] * j;
                    const genNum1 = Math.abs(p1);
                    console.log(genNum1);

                    const genNum2 = Math.abs(p2);
                    console.log(genNum2);

                    let normalized = divideVector(v1, genNum1);
                    // normalized[0] = 0;
                    const interval1 = moveByVector(normalized);
                    const intervalFactor1 = moveByVectorFactor(normalized);
                    const gen1 = Math.abs(interval1 / p2);

                    //console.log('\nV1 normalized: ', normalized);
                    //console.log('\nV1 is moving by', interval1);
                    //console.log('\nV1 as factor', intervalFactor1);
                    // console.log('\nGen from v1:', gen1);
                    //console.log('\ngenNum1: ', genNum1, 'x');
                    console.log('\n>>TOP gen', okt2 - gen1);

                    //normalized1 => prevediem na centy /5
                    //normalized2 => prevediem na centy /13
                    //13x normalized1 -
                    //5x normalized2

                    //console.log('\nV1 generator', moveByVector(normalized) / (v2[0] === 0 ? 1 : v2[0]));

                    console.log('------------');

                    normalized = divideVector(
                        v2,
                        Math.min(Math.abs(v2[0]), Math.abs(v2[2]))
                    );
                    // normalized[0] = 0;
                    const interval2 = moveByVector(normalized);
                    const intervalFactor2 = moveByVectorFactor(normalized);
                    const gen2 = Math.abs(interval2 / p1);
                    //console.log('\nV2 as factor', intervalFactor2);
                    //console.log('\nV2 normalized: ', normalized);
                    //console.log('\nV2 is moving by', interval2);
                    //console.log('\nGen from v2:', gen2);
                    //  console.log('\ngenNum2: ', genNum2, 'x');

                    //console.log('\nV2 generator', moveByVector(normalized) / (v1[0] === 0 ? 1 : v1[0]));
                    console.log('\n>>TOP gen', okt2 - gen2);
                    const m3v2 = Math.abs(p1 + p2);

                    // [1,0,0] [-1,1,0] [-2,0,1] [1,1,-1]

                    // [-9, -6, 7]
                    // [5, 5, -5]
                    // [-10, 0, 5]
                    // [9, 9, -9]
                    // [-23, -5, 14]

                    // [0, 1, -1]
                    // [0, -1, 0]
                    // [0, 0, 1]

                    // [1, 1, 0]
                    // [-1, 0, 1]

                    // [1, 1, -1] // 6/5
                    // [-2,0, 1] // 5/4
                    // [2,0, -1] // 4/5
                    // [0,-1, 1] // 5/3 velka sexta
                    // [3, 0, 1] // 8/5 mala sexta
                    // [-1, 1, 0] // 3/2 kvinta

                    // [-1, -1, 2] // 25/6

                    //5 8 13
                    //const gen = (interval1 * 13) / 65;
                    //console.log('GEN', gen);

                    //const

                    //console.log('\nMala terica v2: ', m3v2);
                    //console.log('\n');
                    // console.log(v1);
                    //console.log(v2);
                    all.push({ g1: gen1, g2: gen2 });
                    // return [okt2 - gen1]; //FIXME: !!!
                }
            }
        }
    }

    const shrinked = all.map((g) => ({
        g1: reduceToPeriod(g.g1, okt2),
        g2: reduceToPeriod(g.g2, okt2),
    }));

    console.log(shrinked);
    return;
};

//const gen = f();

console.log('ahoj\nahoj\rkokot');

function reduceToPeriod(n, p) {
    if (n < 0) {
        let x = n;
        while (x < 0) x += p;
        return x;
    }

    let x = n;
    while (x > p) x -= p;
    return x;
}

//console.log('Generator: ', gen);
