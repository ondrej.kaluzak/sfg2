import { SFG } from '../types/endpoints';
import groupBy from 'lodash.groupby';
import { findGcd } from '../utils/utils';
import { CONTEXT } from '../utils/envConsts';

export const factorToCents = (f: number) => 1200 * Math.log2(f);
export const freqPlusCents = (cents: number, base: number) =>
    base * Math.pow(2, cents / 1200);
const centsToFactor = (c: number) => Math.pow(2, (1 / 1200) * c);

type Vector = [x: number, y: number, z: number];
type Comma = [m: number, n: number];
type CommaCoords = { [prime: string]: number };
type TopTuningParams = {
    numberVector: number[];
    multival: number[];
    factors: Vector;
    aproximants: Vector;
    generator: number;
    period: number;
    periodsInOctave: number;
};

export const genTopTuning = (params: SFG.TuningParams): SFG.Tuning => {
    const topTuning = findTopTuning(params.commaToTemper, params.notTopTuning);
    console.log('Top tuning found: ');
    console.log(topTuning);
    //cely tento if asi zbytocny
    if (topTuning.periodsInOctave !== 1) {
        console.log('\nWarning! Scale is contorted. \n');
        const scale = genScale(
            topTuning.generator,
            topTuning.period,
            Math.floor(params.periodLength / topTuning.periodsInOctave),
            //0 //params.negativeGenerators //FIXME: funguje iba s neg gen = 0
            Math.floor(params.negativeGenerators / topTuning.periodsInOctave)
        );
        const shrinked = shrinkToPeriod(scale, topTuning.period).sort(
            (a, b) => a - b
        );
        let expanded = [...shrinked.slice(0, shrinked.length - 1)];
        console.log('expanded to period');
        console.log(expanded);
        for (var i = 1; i < topTuning.periodsInOctave; i++) {
            expanded = [
                ...expanded,
                ...shrinked
                    .slice(0, shrinked.length - 1)
                    .map((s) => s + i * topTuning.period),
            ];
        }

        const steps: number[] = [];
        for (let s = 0; s < expanded.length - 1; s++) {
            steps.push(expanded[s + 1] - expanded[s]);
        }
        //TODO: ak neni kontorzna scale uzatvorena s napr v 17 tonoch (co asi nikdy) tak dopocitat zvysne kroky

        const missingTonesCount =
            Math.floor(params.periodLength % topTuning.periodsInOctave) + 1;
        const chybajuci =
            (topTuning.aproximants[0] - steps.reduce((acc, c) => acc + c, 0)) /
            missingTonesCount;
        for (let i = 0; i < missingTonesCount; i++) {
            expanded = [...expanded, expanded[expanded.length - 1] + chybajuci];
        }

        console.log('chybajuci');
        console.log(chybajuci);
        //console.log('raw#1 scale');

        // console.log(scale);
        // console.log('raw shrinked');
        // console.log(shrinked);

        console.log('expanded to full octave');
        console.log(expanded);

        const scaleExpanded = expandScale(expanded, params.centerNote);
        // console.log(scaleExpanded);

        const { factors } = topTuning;
        expanded.shift();
        const scalaFormat = expanded;
        return { primes: factors, scale: scaleExpanded, scalaFormat };
    } else {
        // NOT CONTORTED SCALE
        const scale = genScale(
            topTuning.generator,
            topTuning.period,
            params.periodLength / topTuning.periodsInOctave,
            params.negativeGenerators
        );
        console.log('raw scale');
        console.log(scale);

        findMOS(topTuning.generator, topTuning.period, params.periodLength);

        const shrinked = shrinkToPeriod(scale, topTuning.aproximants[0]).sort(
            (a, b) => a - b
        );

        console.log('Reduced scale: ');
        console.log(shrinked.sort((a, b) => a - b));
        console.log('Scale length:', shrinked.length - 1);

        const { factors } = topTuning;

        // only for testing /////
        const steps = [];
        for (let s = 0; s < shrinked.length - 1; s++) {
            steps.push(shrinked[s + 1] - shrinked[s]);
        }
        // only for testing /////

        const scaleExpanded = expandScale(shrinked, params.centerNote);
        const shiftedByCenterNoteShift = scaleExpanded.map(
            (s) => s + params.centerNoteShift
        );
        shrinked.shift();
        const scalaFormat = shrinked;
        return {
            primes: factors,
            scale: shiftedByCenterNoteShift,
            scalaFormat,
            steps,
        };
    }
};

const genScale = (
    generator: number,
    period: number,
    scaleLength: number,
    negGens: number
) => {
    const min = -negGens;
    const max = scaleLength - negGens;
    const scale = [];
    for (var i = min; i < max; i++) {
        scale.push(i * generator);
    }
    return scale;
};

const findMOS = (generator: number, period: number, desiredLength: number) => {
    let isMos = false;
    let fallback = 100;
    let i = 1;
    const allMosses: { length: number; steps: string[] }[] = [];
    console.log('\n\nSearching for moment of symetry... ');
    while (fallback) {
        const scale = genScale(generator, 0, i, 0); //FIXME: period = 0
        const shrinked = shrinkToPeriod(scale, period).sort((a, b) => a - b);
        const steps: number[] = new Array();
        for (let s = 0; s < shrinked.length - 1; s++) {
            steps.push(shrinked[s + 1] - shrinked[s]);
        }

        const groupped = groupBy(steps, (s) => Math.round(s * 10000) / 10000);
        const keys = Object.keys(groupped);
        if (keys.length <= 2) {
            allMosses.push({ length: shrinked.length - 1, steps: keys });
            if (desiredLength === shrinked.length - 1) {
                isMos = true;
            }
        }

        i++;
        fallback--;
    }
    if (!isMos) {
        console.log(`Warning! ${desiredLength} tones will not make 2D scale.`);
        CONTEXT.NO_MOS = true;
    }

    console.log('\nMOS periods: ');
    console.log(allMosses);
};

const shrinkToPeriod = (scale: number[], period: number) => {
    const shrinked = scale.map((s) => {
        if (s === 0) return s;
        if (s < 0) {
            while (s < 0) {
                s += period;
            }
            return s;
        } else {
            while (s >= period) {
                s -= period;
            }
            return s;
        }
    });

    /* const uniqueArray = shrinked.filter(function (item, pos, self) {
        return self.indexOf(item) == pos;
    }); */
    return [...shrinked, period];
};

const expandScale = (scale: number[], rootNote: SFG.MidiNote) => {
    const expanded: number[] = new Array(128);
    const periodLength = scale.length - 1;
    const steps = new Array();
    for (let s = 0; s < scale.length - 1; s++) {
        steps.push(scale[s + 1] - scale[s]);
    }
    console.log('Steps:');
    console.log(steps);

    expanded[rootNote] = 0.0;
    for (let k = 1; k < expanded.length - rootNote; k++) {
        const index = k + rootNote;
        expanded[index] = expanded[index - 1] + steps[(k - 1) % periodLength];
    }

    for (let k = 0; k < rootNote; k++) {
        const index = rootNote - 1 - k;
        expanded[index] =
            expanded[index + 1] - steps[steps.length - 1 - (k % periodLength)];
    }

    return expanded;
};

function getAllFactorsFor(remainder: number) {
    var factors: number[] = [];
    for (var i = 2; i <= remainder; i++) {
        while (remainder % i === 0) {
            factors.push(i);
            remainder /= i;
        }
    }
    return factors;
}

const removeCommonDivisors = (comma: Comma): Comma => {
    let x = Math.abs(comma[0]);
    let y = Math.abs(comma[1]);
    while (y > 0) {
        let t = y;
        y = x % y;
        x = t;
    }
    return [comma[0] / x, comma[1] / x] as Comma;
};

const findPrimesCoordinates = (comma: Comma): Vector => {
    const c = removeCommonDivisors(comma);
    if (!isValidComma(c)) {
        console.log('\nInvalid comma.\n');
        process.exit(1);
    }

    const c1: { [key: string]: number } = {};
    const c2: { [key: string]: number } = {};
    // rozloz na prvocisla a napln poctom objekty c1 a c2 - comma rozlozene na prvocinitele
    getAllFactorsFor(c[0]).forEach((x) => (c1[x] = (c1[x] || 0) + 1));
    getAllFactorsFor(c[1]).forEach((x) => (c2[x] = (c2[x] || 0) + 1));
    const C1 = Math.max(...getAllFactorsFor(c[0]));
    const C2 = Math.max(...getAllFactorsFor(c[1]));
    if (C1 > 5 || C2 > 5) {
        console.log('\n Only 5-limit tunings are supported.\n');
        process.exit(1);
    }

    let coordsObj: CommaCoords = { '2': 0, '3': 0, '5': 0 };

    Object.keys(c1).forEach((k) => (coordsObj[k] = c1[k]));
    Object.keys(c2).forEach((k) => (coordsObj[k] = c2[k] * -1));
    console.log(coordsObj);
    const sortedKeys = Object.keys(coordsObj).sort(
        (a, b) => Number.parseInt(a) - Number.parseInt(b)
    );
    const numberVector = sortedKeys.map((k) => coordsObj[k]);
    return numberVector as Vector;
};

const findTopTuning = (
    comma: Comma,
    notTopTuning?: boolean
): TopTuningParams => {
    const numberVector = findPrimesCoordinates(comma);

    const multival = numberVector.map((n) => Math.abs(n)).reverse();
    const { aproximants, uncomplete } = getAproximants(
        comma,
        numberVector,
        notTopTuning
    );
    if (notTopTuning) {
        aproximants[0] = 1200;
        aproximants[1] = factorToCents(3);
        aproximants[2] = factorToCents(5);
    }
    const gcdAll = findGcd(numberVector);
    const gcdRest = findGcd([numberVector[1], numberVector[2]]);
    let gcdFactor = gcdRest / gcdAll;
    gcdFactor = 1;
    let period = aproximants[0] / gcdFactor;

    const factors = aproximants.map((c) => centsToFactor(c)) as Vector;

    if (uncomplete) {
        const v = numberVector;
        const Oktava = 1200;
        const Duodecima = factorToCents(3);
        const Septemdecima = factorToCents(5);
        console.log('Incomplete vector detected!');
        let generator = 100;
        let newPeriod = period;
        if (v[0] === 0) {
            newPeriod = Oktava;
            const amount = Math.abs(v[2]);
            generator = Math.abs(
                (aproximants[1] - amount * newPeriod) / amount
            );
            //ASI ZBYTOCNE
            //gcdFactor = amount;
            console.log(newPeriod, generator, newPeriod - generator);
        }
        if (v[1] === 0) {
            newPeriod = period;
            //ASI ZBYTOCNE
            // newPeriod = Math.abs(period / v[2]);
            // gcdFactor = Math.abs(v[2]);
            const maxPeriod = Math.floor(Duodecima / newPeriod);
            generator = Duodecima - newPeriod * maxPeriod;
            console.log(newPeriod, generator, newPeriod - generator);
        }
        if (v[2] === 0) {
            newPeriod = period;
            //ASI ZBYTOCNE
            //newPeriod = Math.abs(period / v[1]);
            // gcdFactor = Math.abs(v[1]);
            const maxPeriod = Math.floor(aproximants[2] / newPeriod);
            generator = Septemdecima - newPeriod * maxPeriod;
            console.log(newPeriod, generator, newPeriod - generator);
        }
        return {
            numberVector,
            multival,
            aproximants,
            factors,
            generator, //: newPeriod - generator,
            period: newPeriod,
            periodsInOctave: gcdFactor,
        };
    }

    const generator = reduceToPeriod(
        findGenerator(aproximants, numberVector, gcdFactor, period),
        period
    );

    return {
        numberVector,
        multival,
        aproximants,
        factors,
        generator,
        period,
        periodsInOctave: gcdFactor,
    };
};

const getAproximants = (
    comma: Comma,
    numberVector: Vector,
    notTopTuning?: boolean
): { aproximants: Vector; uncomplete: boolean } => {
    const Oktava = 1200;
    const Duodecima = factorToCents(3);
    const Septemdecima = factorToCents(5);
    /* if (notTopTuning) {
        return {
            aproximants: [Oktava, Duodecima, Septemdecima],
            uncomplete: true,
        };
    } */
    let x = Oktava;
    let y = Duodecima;
    let z = Septemdecima;
    let uncomplete = false;

    const commaInCents = factorToCents(comma[0] / comma[1]);
    const commaFactorLog = Math.log(comma[0] * comma[1]);

    if (numberVector[0] === 0) {
        uncomplete = true;
    } else {
        const signOf2 = -1 * (numberVector[0] / Math.abs(numberVector[0]));
        x =
            factorToCents(2) +
            signOf2 * ((commaInCents * Math.log(2)) / commaFactorLog);
    }

    if (numberVector[1] === 0) {
        uncomplete = true;
    } else {
        const signOf3 = -1 * (numberVector[1] / Math.abs(numberVector[1]));
        y =
            factorToCents(3) +
            signOf3 * ((commaInCents * Math.log(3)) / commaFactorLog);
    }

    if (numberVector[2] === 0) {
        uncomplete = true;
    } else {
        const signOf5 = -1 * (numberVector[2] / Math.abs(numberVector[2]));
        z =
            factorToCents(5) +
            signOf5 * ((commaInCents * Math.log(5)) / commaFactorLog);
    }
    return { aproximants: [x, y, z], uncomplete };
};

const findGenerator = (
    aproximants: Vector,
    numberVector: Vector,
    gcdFactor: number,
    period: number
): number => {
    const moveByVector = (v: Vector) =>
        v[0] * period + v[1] * aproximants[1] + v[2] * aproximants[2];

    const v1: Vector = [0, numberVector[1] / gcdFactor, 0];
    const v2: Vector = [0, 0, numberVector[2] / gcdFactor];

    const p0 = numberVector[0];
    const p1 = numberVector[1] / gcdFactor;
    const p2 = numberVector[2] / gcdFactor;
    const MAX_PROLONG = 3;
    for (let prolong = 1; prolong < MAX_PROLONG; prolong++) {
        for (
            let i = -1 * Math.abs(p2) * prolong;
            i < Math.abs(p2) * prolong;
            i++
        ) {
            for (
                let j = -1 * Math.abs(p1) * prolong;
                j < Math.abs(p1) * prolong;
                j++
            ) {
                if (p1 * i + p2 * j === p0) {
                    v1[0] = v1[1] * i;
                    v2[0] = v2[2] * j;
                    const genNum1 = Math.abs(p1);
                    const genNum2 = Math.abs(p2);
                    let normalized = divideVector(v1, genNum1);
                    const interval1 = moveByVector(normalized);
                    const gen1 = Math.abs(interval1 / p2);
                    return gen1;
                }
            }
        }
    }
    //TODO: neviem ci je uplne stastne riesenie, lisi sa to v podmienke if(...)
    // for (let i = -1 * Math.abs(p2); i < Math.abs(p2); i++) {
    //     for (let j = -1 * Math.abs(p1); j < Math.abs(p1); j++) {
    //         if (p1 * j + p2 * i === p0) {
    //             v1[0] = v1[1] * i;
    //             v2[0] = v2[2] * j;
    //             const genNum1 = Math.abs(p1);
    //             const genNum2 = Math.abs(p2);
    //             let normalized = divideVector(v1, genNum1);
    //             const interval1 = moveByVector(normalized);
    //             const gen1 = Math.abs(interval1 / p2);
    //             return gen1;
    //         }
    //     }
    // }
    console.log('No standart solution found! Using tempered 5th as generator');

    return aproximants[1] - aproximants[0];
};

export const divideVector = (v: Vector, k: number): Vector => {
    let kk = Math.abs(k);
    if (kk === 0) kk = 1;

    return [v[0] / kk, v[1] / kk, v[2] / kk];
};

export function reduceToPeriod(generator: number, periodAprox: number) {
    if (generator < 0) {
        let x = generator;
        while (x < 0) x += periodAprox;
        return x;
    }

    let x = generator;
    while (x > periodAprox) x -= periodAprox;
    return x;
}

const isValidComma = (comma: Comma): boolean => comma[0] !== comma[1];
