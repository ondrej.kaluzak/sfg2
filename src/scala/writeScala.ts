import fs from 'fs';
import { SFG } from '../types/endpoints';
import { CONTEXT } from '../utils/envConsts';

export const writeScalaFile = (
    path: string,
    name: string,
    scale: number[],
    tuningParams: SFG.TuningParams
) => {
    if (!fs.existsSync(path)) {
        fs.mkdirSync(path);
    }

    const scalaPath = `${path}`;
    if (!fs.existsSync(scalaPath)) {
        fs.mkdirSync(scalaPath);
    }

    const length = scale.length.toString();
    console.log(`Writing ${name} scala file to ${path}...`);
    //TODO: implement aditional informations about scale - used generator, steps, aproximants, number vector
    const sclHeader = `! ${name}.scl\n!${
        CONTEXT.NO_MOS ? `\n! Warning: this is not two-dimensional scale.` : ''
    }\nTop scale ${name} with tempered interval ${
        tuningParams.commaToTemper[0]
    }/${tuningParams.commaToTemper[1]}.\n${length}\n!\n`;

    let sclString = sclHeader;

    scale.forEach((n) => {
        sclString += n.toString() + '\n';
    });

    fs.writeFileSync(`${scalaPath}/${name}.scl`, sclString);
};
