import fs from 'fs';
import { SFG } from '../types/endpoints';
import {
    ADD_PRESET_COMMENTS,
    EMPTY_PRESET,
    EOL,
    MAX_ALLOWED_ATTACK,
    MAX_ALLOWED_PARTIALS,
    OUTPUT_DIR,
    PRESET_CMT,
    TESTING_MODE,
} from '../utils/envConsts';
import { ERROR, INFO, sanitizeName, WARNING } from '../utils/utils';
import { validatePreset } from './validatePreset';

const PRESET_PATH_PREFIX = OUTPUT_DIR;

export const writePreset = (
    fileName: string,
    preset: SFG.Preset,
    format: 'JSON' | 'SFG' = 'JSON'
) => {
    if (format === 'JSON') {
        fs.writeFileSync(
            `${PRESET_PATH_PREFIX}/${fileName}.json`,
            JSON.stringify(preset),
            { encoding: 'utf-8' }
        );
        return;
    }
    const sfgPreset = convertToSfg(preset);
    fs.writeFileSync(`${PRESET_PATH_PREFIX}/${fileName}.txt`, sfgPreset, {
        encoding: 'utf-8',
    });
    return;
};

export const readPreset = (
    fileName: string,
    format: 'JSON' | 'SFG' = 'JSON'
): SFG.Preset => {
    if (format === 'JSON') {
        const json = fs.readFileSync(
            `${PRESET_PATH_PREFIX}/${fileName}.json`,
            'utf-8'
        );
        const parsed = JSON.parse(json) as SFG.Preset;
        return parsed;
    }
    try {
        let str = '';
        const extension = fileName.slice(fileName.length - 4, fileName.length);
        const filePath = `${PRESET_PATH_PREFIX}/${fileName}`;
        if (extension === '.txt') {
            str = fs.readFileSync(filePath, 'utf-8');
        } else {
            str = fs.readFileSync(`${filePath}.txt`, 'utf-8');
        }
        if (TESTING_MODE) {
            console.log('**********TESTING MODE OUTPUT**************');
            console.log(parseSfgPreset(str));
            console.log('********TESTING MODE OUTPUT END************');
        }
        return parseSfgPreset(str);
    } catch (error) {
        console.log('\nUnable to load or parse the preset.\n');
        process.exit();
    }
};

const com = (comment: string) =>
    ADD_PRESET_COMMENTS ? EOL + PRESET_CMT + comment + EOL : '';

const convertToSfg = (p: SFG.Preset) => {
    let str = '';
    str += 'NAME ' + p.name + EOL;
    str += com('Top tuning and scale settings:');
    str += 'NUMERATOR ' + p.tuningParams.commaToTemper[0] + EOL;
    str += 'DENOMINATOR ' + p.tuningParams.commaToTemper[1] + EOL;
    str += 'NOTES_IN_PERIOD ' + p.tuningParams.periodLength + EOL;
    str += 'NEGATIVE_GENERATORS ' + p.tuningParams.negativeGenerators + EOL;
    str += 'CENTER_NOTE ' + p.tuningParams.centerNote + EOL;
    str += 'CENTER_NOTE_SHIFT ' + p.tuningParams.centerNoteShift + EOL;
    str += com('Partials settings:');
    str += 'PARTIALS_COUNT ' + p.partialsSettings.count + EOL;
    str += 'PARTIALS_ROLL_OFF ' + p.partialsSettings.attenPower + EOL;
    str += com('Sound envelope settings:');
    str += 'ATTACK ' + p.mainEnv.attack + EOL;
    str += 'SUSTAIN ' + p.mainEnv.sustain + EOL;
    str += 'RELEASE ' + p.mainEnv.release + EOL;
    str += 'COMMON_DECAY_NOTE ' + p.mainEnv.decay.commonDecayNote + EOL;
    str += 'DECAY_ATTENUATION_TIME_UNIT ' + p.mainEnv.decay.timeUnit + EOL;
    str +=
        'DECAY_ATTENUATION_PER_TIME_UNIT ' +
        p.mainEnv.decay.dbPerTimeUnit +
        EOL;
    str += 'DECAY_MAX_LENGTH ' + p.mainEnv.decay.maxDecayLength + EOL;
    str += 'DECAY_POWER ' + p.mainEnv.decay.speedPower + EOL;
    str += com('SFZ soundfont settings:');
    str += 'SFZ_SUS_LOOP_LENGTH ' + p.mainEnv.sfzSus?.loopLengthMs + EOL;
    str += 'SFZ_CROSSFADE_LENGTH ' + p.mainEnv.sfzSus?.crossFadeMs + EOL;
    // str += 'DECAY ' + p.mainEnv.decay + EOL;
    // str += 'ATTACK_STEEPNESS ' + p.mainEnv.attackSteep + EOL;
    // str += 'DECAY_STEEPNESS ' + p.mainEnv.decaySteep + EOL;
    // str += 'RELEASE_STEEPNESS ' + p.mainEnv.releaseSteep + EOL;

    //str += `SFZ_LOOPED_INSTRUMENT ${p.sfzLoopedInstrument ? 1 : 0}\r\n`;
    return str;
};

const parseSfgPreset = (str: string) => {
    const parsed = str.split(/\r?\n/);

    //console.log(parsed);
    const preset: SFG.Preset = { ...EMPTY_PRESET } as SFG.Preset;
    parsed.forEach((line) => parseLine(line, preset));
    //console.log(preset);
    validatePreset(preset);
    return preset;
};

const parseLine = (line: string, preset: SFG.Preset) => {
    if (line.replaceAll(' ', '')[0] === '!') {
        if (TESTING_MODE && line.replaceAll(' ', '').length > 1)
            INFO(`Preset comment: ${line}`);
        return;
    }
    let i = 0;
    let paramName = '';
    let value = '';
    while (line[i] !== ' ' && i < line.length) {
        paramName += line[i];
        i++;
    }
    value = line.slice(i, line.length);
    switch (paramName) {
        case 'NAME':
            preset.name = sanitizeName(
                value.replaceAll(' ', '') || 'unknown_name'
            );
            break;
        case 'NUMERATOR':
            preset.tuningParams.commaToTemper[0] = parseInt(value);
            break;
        case 'DENOMINATOR':
            preset.tuningParams.commaToTemper[1] = parseInt(value);
            break;
        case 'NOTES_IN_PERIOD':
            preset.tuningParams.periodLength = parseInt(value);
            break;
        case 'NEGATIVE_GENERATORS':
            preset.tuningParams.negativeGenerators = parseInt(value);
            break;
        case 'CENTER_NOTE':
            preset.tuningParams.centerNote = parseInt(value) as SFG.MidiNote;
            break;
        case 'CENTER_NOTE_SHIFT':
            preset.tuningParams.centerNoteShift = parseFloat(value);
            break;
        case 'ATTACK':
            preset.mainEnv.attack = parseInt(value);
            break;
        case 'SUSTAIN':
            preset.mainEnv.sustain = parseFloat(value);
            if (preset.mainEnv.sustain === 0) preset.mainEnv.mode = 'PERC';
            break;
        case 'RELEASE':
            preset.mainEnv.release = parseInt(value);
            break;
        case 'PARTIALS_COUNT':
            preset.partialsSettings.count = parseInt(value);
            break;
        case 'PARTIALS_ROLL_OFF':
        case 'PARTIALS_ATTENUATION_POWER':
            preset.partialsSettings.attenPower = parseFloat(value);
            break;
        case 'SFZ_SUS_LOOP_LENGTH':
            preset.mainEnv.sfzSus.loopLengthMs = parseInt(value);
            break;
        case 'SFZ_CROSSFADE_LENGTH':
            preset.mainEnv.sfzSus.crossFadeMs = parseInt(value);
            break;
        case 'COMMON_DECAY_NOTE':
            preset.mainEnv.decay.commonDecayNote = parseInt(value);
            break;
        case 'DECAY_ATTENUATION_TIME_UNIT':
            preset.mainEnv.decay.timeUnit = parseInt(value);
            break;
        case 'DECAY_ATTENUATION_PER_TIME_UNIT':
            preset.mainEnv.decay.dbPerTimeUnit = parseInt(value);
            break;
        case 'DECAY_MAX_LENGTH':
            preset.mainEnv.decay.maxDecayLength = parseInt(value);
            break;
        case 'DECAY_POWER':
            preset.mainEnv.decay.speedPower = parseFloat(value);
            break;

        default:
            if (line.length > 0) WARNING(`Unparsed preset line: ${line}`);
            break;
    }
};
