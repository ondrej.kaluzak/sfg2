import { WaveFile } from 'wavefile';
import { SFG } from '../types/endpoints';
import fs from 'fs';
import { ERROR } from '../utils/utils';

export const writeWaveFile = (
    path: string,
    chans: number,
    sampleRate: SFG.SampleRateType,
    bitDepth: SFG.BitDepthType,
    data: SFG.Wave | [SFG.Wave, SFG.Wave]
): void => {
    let wav = new WaveFile();
    wav.fromScratch(chans, sampleRate, bitDepth, data);
    wav.toBitDepth('16', true);
    let success = false;
    let i = 1;
    while (!success && i < 10) {
        try {
            fs.writeFileSync(path, wav.toBuffer());
            success = true;
        } catch (error) {
            ERROR(`Unable to write wave file. Trying different name...`);
            path = path.slice(0, path.length - 4);
            path = path + `(${i}).wav`;
            i++;
        }
    }
    if (!success) ERROR(`Unable to write wave file.`);

    process.stdout.write(`\rFile ${path} written to disc.`);
};

export const convertToWavFileFormat = (
    chans: number,
    sampleRate: SFG.SampleRateType,
    bitDepth: SFG.BitDepthType,
    data: SFG.Wave | [SFG.Wave, SFG.Wave]
): WaveFile => {
    let wav = new WaveFile();
    wav.fromScratch(chans, sampleRate, bitDepth, data);
    return wav;
};
