import { SFG } from '../types/endpoints';
import { MAX_ALLOWED_PARTIALS } from '../utils/envConsts';
import { ERROR, isInt, isNan, LOG, WARNING } from '../utils/utils';

export const validatePreset = (preset: SFG.Preset) => {
    validateTunningParams(preset);
    validateEnvelope(preset);
    validatePartialsSettings(preset);
};

const validateEnvelope = (preset: SFG.Preset) => {
    const { mainEnv } = preset;
    const { attack, release, decay, sustain, sfzSus } = mainEnv;
    let fullSustain = false;
    if (isNan(attack) || attack <= 0) {
        //WARNING(`Attack is negative number, setting to 0.`);
        preset.mainEnv.attack = 0.1;
    }
    if (isNan(release) || release <= 0) {
        //WARNING(`Release is negative number, setting to 0.`);
        preset.mainEnv.release = 0.1;
    }
    if (isNan(sustain) || sustain < 0) {
        WARNING(`Sustain is negative number, setting to 0.`);
        preset.mainEnv.sustain = 0;
    }

    if (sustain > 1) {
        WARNING(`Sustain can not be greater than 1, setting to 1.`);
        preset.mainEnv.sustain = 1;
        fullSustain = true;
    }
    //TODO:
    if (sustain >= 1) {
        preset.mainEnv.sustain = 0.9999;
        fullSustain = true;
        console.log('Full sustain');
    }
    if (
        isNan(decay.commonDecayNote) ||
        decay.commonDecayNote < 0 ||
        decay.commonDecayNote > 127
    ) {
        WARNING(`COMMON_DECAY_NOTE is invalid. Setting to default 60.`);
        preset.mainEnv.decay.commonDecayNote = 60;
    }

    if (isNan(decay.speedPower) || decay.speedPower < 0) {
        WARNING(
            `DECAY_POWER has to be floating point number greater or equal to 0. Setting to default 1.0`
        );
        preset.mainEnv.decay.speedPower = 1;
    }

    if (isNan(decay.maxDecayLength) || decay.maxDecayLength < 10) {
        WARNING(
            `DECAY_MAX_LENGTH has to be an integer number greater than 10. Setting to default 30000 ms.`
        );
        preset.mainEnv.decay.maxDecayLength = 30000;
    }

    if (isNan(decay.dbPerTimeUnit) || decay.dbPerTimeUnit < 0) {
        if (isNan(decay.dbPerTimeUnit)) {
            WARNING(
                `DECAY_ATTENUATION_PER_TIME_UNIT has to be positive number. Setting to default value 3 dB.`
            );
            preset.mainEnv.decay.dbPerTimeUnit = 3;
        } else {
            WARNING(
                `DECAY_ATTENUATION_PER_TIME_UNIT has to be positive number. Setting to absolute value.`
            );
            preset.mainEnv.decay.dbPerTimeUnit = Math.abs(decay.dbPerTimeUnit);
        }
    }
    //TODO:
    if (fullSustain) {
        preset.mainEnv.decay.dbPerTimeUnit = 0.1;
        console.log('Full sustain, so decay is: ');
        LOG(preset.mainEnv.decay.dbPerTimeUnit);
    }

    if (isNan(decay.timeUnit) || decay.timeUnit < 0) {
        if (isNan(decay.timeUnit)) {
            WARNING(
                `DECAY_ATTENUATION_TIME_UNIT has to be positive number. Setting to default value 500 ms.`
            );
            preset.mainEnv.decay.timeUnit = 500;
        } else {
            WARNING(
                `DECAY_ATTENUATION_TIME_UNIT has to be positive number. Setting to absolute value ${Math.abs(
                    decay.timeUnit
                )} ms.`
            );
            preset.mainEnv.decay.timeUnit = Math.abs(decay.timeUnit);
        }
    }
    if (isNan(sustain) || sfzSus.crossFadeMs < 100) {
        WARNING(
            `SFZ_CROSSFADE_LENGTH has to be greater than 100 ms. Setting to 100 ms.`
        );
        preset.mainEnv.sfzSus.crossFadeMs = 100;
    }
    if (
        !isInt(sfzSus.loopLengthMs) ||
        sfzSus.loopLengthMs < preset.mainEnv.sfzSus.crossFadeMs
    ) {
        WARNING(
            `SFZ_SUS_LOOP_LENGTH has to be greater or equal to SFZ_CROSSFADE_LENGTH. Setting to ${preset.mainEnv.sfzSus.crossFadeMs} ms.`
        );
        preset.mainEnv.sfzSus.loopLengthMs = preset.mainEnv.sfzSus.crossFadeMs;
    }
};

const validatePartialsSettings = (preset: SFG.Preset) => {
    const { partialsSettings } = preset;
    const { attenPower, count } = partialsSettings;
    if (isNan(count) || count < 0 || count > MAX_ALLOWED_PARTIALS) {
        WARNING(
            `PARTIALS_COUNT has to be less than ${MAX_ALLOWED_PARTIALS} and greater than 0. Setting to default 50.`
        );
        preset.partialsSettings.count = 50;
    }

    if (isNan(attenPower) || attenPower < 0) {
        WARNING(
            `PARTIALS_ROLL_OFF has to be greater or equal to 0. Setting to default 1.`
        );
        preset.partialsSettings.attenPower = 1;
    }
};

const validateTunningParams = (preset: SFG.Preset) => {
    const { tuningParams } = preset;
    const {
        centerNote,
        centerNoteShift,
        commaToTemper,
        negativeGenerators,
        periodLength,
    } = tuningParams;
    if (isNan(centerNote) || centerNote < 0 || centerNote > 127) {
        WARNING(
            `CENTER_NOTE has to be valid MIDI note. Setting to default 60.`
        );
        preset.tuningParams.centerNote = 60;
    }
    if (isNan(centerNoteShift)) {
        WARNING(
            `CENTER_NOTE_SHIFT has to be valid floating point number. Setting to default 0 cents.`
        );
        preset.tuningParams.centerNoteShift = 0;
    }
    if (
        isNan(negativeGenerators) ||
        !isInt(negativeGenerators) ||
        negativeGenerators < 0
    ) {
        WARNING(
            `NEGATIVE_GENERATORS has to be integer greater than 0. Setting to default 0.`
        );
        preset.tuningParams.negativeGenerators = 0;
    }
    if (isNan(periodLength) || periodLength <= 0) {
        WARNING(
            `NOTES_IN_PERIOD has to be greater than 0. Setting to default 12.`
        );
        preset.tuningParams.periodLength = 12;
    }
    /**COMMA */
    if (isNan(commaToTemper[0]) || isNan(commaToTemper[1])) {
        ERROR(`Invalid NUMERATOR or DENOMINATOR of comma to temper. Exitting,`);
        process.exit(1);
    }
    if (commaToTemper[0] === commaToTemper[1]) {
        ERROR(
            `NUMERATOR and DENOMINATOR of comma to temper are the same number.`
        );
        process.exit(1);
    }

    if (!isInt(commaToTemper[0]) || !isInt(commaToTemper[1])) {
        ERROR(`NUMERATOR or DENOMINATOR is not an integer number.`);
        process.exit(1);
    }
    if (commaToTemper[0] <= 0 || commaToTemper[1] <= 0) {
        ERROR(`NUMERATOR and DENOMINATOR has to be greater than 0.`);
        process.exit(1);
    }
};
