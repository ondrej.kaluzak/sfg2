export function noteToFreq(note: number) {
    let a = 440; //frequency of A (coomon value is 440Hz)
    return (a / 32) * (2 ** ((note - 9) / 12));
}


export const freqPlusCents = (cents: number, base: number) => base * Math.pow(2, (cents / 1200));

export function centsToFreq(root: number, cents: number) {
    return freqPlusCents(cents, root);

}

