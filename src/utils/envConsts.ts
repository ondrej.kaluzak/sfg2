import { noteToFreq } from '../midi/noteToFreq';
import { SFG } from '../types/endpoints';
import { dbToFactor, msToSmps } from './utils';
/**MAIN - TEST MODE */
if (process.env.TESTING_MODE === undefined) {
    process.env.TESTING_MODE = 'false';
}
export const TESTING_MODE = process.env.TESTING_MODE === 'true';
export const TEST_PARAMS_ON = TESTING_MODE;
/**MATH AND SOUND SETTINGS */
export const SR: SFG.SampleRateType = 44100;
export const OUT_SR: SFG.SampleRateType = 44100;
export const BR: SFG.BitDepthType = '32f';
export const OUT_BR: SFG.BitDepthType = '16';
export const PI2 = Math.PI * 2;
export const MAX_ALLOWED_PARTIALS = 100;
export const SFZ_PANNING = true;
export const ALLOW_MIDI_STEREO = true;
/**FOLDERS */
export const OUTPUT_DIR = TESTING_MODE ? './binary' : '.';
export const OUTPUT_WAVS = TESTING_MODE ? 'wavs' : '.';
/**PRESETS */
export const PRESET_MODE: 'SFG' | 'JSON' = 'SFG';
export const EOL = '\r\n';
export const PRESET_CMT = '! ';
export const ADD_PRESET_COMMENTS = true;
/**CONSOLE OUTPUT */
export const CONSOLE_USE_COLORS = TESTING_MODE;
export const CONSOLE_OUTPUT_DETAILS = true;
/**SOUND/ENVELOPE */
export const SILENCE_dB = -80;
export const MAX_ALLOWED_DECAY_TIME = 80000;
export const DEFAULT_ALLOWED_DECAY_TIME = 30000;
export const SILENCE_FACTOR = dbToFactor(SILENCE_dB);
export const MAX_ALLOWED_DECAY_TIME_SMPS = msToSmps(MAX_ALLOWED_DECAY_TIME);
export const MIDI_TEMPO_FACTOR = TESTING_MODE ? 1 : 1;
export const MIN_FREQ_TO_RENDER = 8;
export const MAX_FREQ_TO_RENDER = SR / 2;
export const MAX_ALLOWED_ATTACK = 60000;

/**GLOBAL CONTEXT */
export const CONTEXT = {
    NO_MOS: false,
};

const PercusiveEnv: SFG.Envelope = {
    attack: 5,
    sustain: 0,
    decay: {
        dbPerTimeUnit: 6,
        maxDecayLength: DEFAULT_ALLOWED_DECAY_TIME,
        timeUnit: 200,
        speedPower: 1,
        commonDecayNote: 60,
        commonDecayFreq: noteToFreq(60),
        dbSpeedPerSec: 6,
    },
    mode: 'PERC',

    sfzSus: {
        loopLengthMs: 4000,
        crossFadeMs: 2500,
    },

    release: 10,
    data: null as unknown as SFG.EnvData,
};

const SustainedEnv: SFG.Envelope = {
    attack: 30,
    sustain: 0.5,
    release: 1000,
    decay: {
        dbPerTimeUnit: 6,
        maxDecayLength: DEFAULT_ALLOWED_DECAY_TIME,
        timeUnit: 500,
        speedPower: 1,
        commonDecayNote: 60,
        commonDecayFreq: noteToFreq(60),
        dbSpeedPerSec: 6,
    },
    mode: 'SFZLOOP',

    sfzSus: {
        loopLengthMs: 4000,
        crossFadeMs: 2500,
    },
    data: null as unknown as SFG.EnvData,
};

//empty preset for SFGP presets
export const EMPTY_PRESET: SFG.Preset = {
    name: '',
    tuningParams: {
        commaToTemper: [0, 0],
        periodLength: 0,
        negativeGenerators: 0,
        centerNote: null as unknown as SFG.MidiNote,
        centerNoteShift: undefined as unknown as number,
        notTopTuning: false,
    },
    mainEnv: { ...SustainedEnv },

    partialsSettings: {
        count: 50,
        attenPower: 1,
        partialsEnv: [],
    },
};

export const DEFAULT_PRESET_SETTINGS: SFG.Preset = {
    name: 'defaultPreset',

    tuningParams: {
        commaToTemper: [81, 80],
        periodLength: 12,
        negativeGenerators: 5,
        centerNote: 69,
        centerNoteShift: 0,
        notTopTuning: false,
    },

    mainEnv: { ...SustainedEnv },

    partialsSettings: {
        count: 50,
        attenPower: 1,
        partialsEnv: [],
    },
};

export const MEANTONE_PRESET: SFG.Preset = {
    ...DEFAULT_PRESET_SETTINGS,
    name: 'meantone',
    mainEnv: { ...SustainedEnv },

    tuningParams: {
        commaToTemper: [81, 80],
        periodLength: 12,
        negativeGenerators: 5,
        centerNote: 69,
        centerNoteShift: 0,
        notTopTuning: false,
    },
};

export const HANSON_PRESET: SFG.Preset = {
    ...DEFAULT_PRESET_SETTINGS,
    name: 'hanson',
    tuningParams: {
        commaToTemper: [15625, 15552],
        periodLength: 11,
        negativeGenerators: 3,
        centerNote: 67,
        centerNoteShift: 0,
        notTopTuning: false,
    },
};

export const MAVILA_PRESET: SFG.Preset = {
    ...DEFAULT_PRESET_SETTINGS,
    name: 'mavila',
    mainEnv: { ...PercusiveEnv },
    tuningParams: {
        commaToTemper: [135, 128],
        periodLength: 7,
        negativeGenerators: 2,
        centerNote: 60,
        centerNoteShift: 0,
        notTopTuning: false,
    },
};

export const AMITY_PRESET: SFG.Preset = {
    ...DEFAULT_PRESET_SETTINGS,
    name: 'amity',
    tuningParams: {
        commaToTemper: [1600000, 1594323],
        periodLength: 18,
        negativeGenerators: 5,
        centerNote: 60,
        centerNoteShift: 0,
        notTopTuning: false,
    },
};

export const PYTH_PRESET: SFG.Preset = {
    ...DEFAULT_PRESET_SETTINGS,
    name: 'pyth',
    tuningParams: {
        commaToTemper: [3, 2],
        periodLength: 12,
        negativeGenerators: 0,
        centerNote: 60,
        centerNoteShift: 0,
        notTopTuning: false,
    },
};

export const DEFAULT_PRESET = MEANTONE_PRESET;

export const ALL_PRESETS = [
    MEANTONE_PRESET,
    HANSON_PRESET,
    MAVILA_PRESET,
    AMITY_PRESET,
];

type NoiseHit = {
    noiseWave: SFG.Wave;
};
