import {
    ControllerEvent,
    MidiFile,
    NoteOffEvent,
    NoteOnEvent,
    SetTempoEvent,
} from 'midifile-ts';
import { genSilence, multiply, normalize } from '../gens/basicGenerators';
import { createEnvelope } from '../gens/createEnvelope';
import { newComplexTone, PulseProps } from '../gens/toneNextGen';

import { createTopTuning } from '../tunings/createTopTuning';
import { SFG } from '../types/endpoints';
import {
    BR,
    MIDI_TEMPO_FACTOR,
    OUTPUT_DIR,
    OUTPUT_WAVS,
    SR,
    TESTING_MODE,
} from '../utils/envConsts';
import { checkAndMakeDir, ERROR, LOG, msToSmps } from '../utils/utils';
import { writeWaveFile } from '../wavefiles';
import { loadMidiFile } from './loadMidiFile';
import { freqPlusCents, noteToFreq } from './noteToFreq';

//const ONE_DELTA = 1 / 380; //ONE DELTA IN SECONDS

//TODO: CLEAN UP, fix all weird stuff

const TEMPO_FACTOR = MIDI_TEMPO_FACTOR;
const MONO_AMMOUNT = 1;

export const renderMidiFile = (
    preset: SFG.Preset,
    pathMidi: string,
    outputFileName: string,
    stereoMode: boolean
) => {
    const { allParsedTracks, maxTime } = parseMidiEvents(pathMidi);
    console.log(
        '\nSong time:\n',
        maxTime,
        ' ms\n' +
            Math.floor(maxTime / 1000) +
            ' s\n' +
            Math.floor(maxTime / 600) / 100 +
            'min\n\n'
    );

    const sbox = makeSandbox(
        Math.floor(stereoMode ? 0 : maxTime + preset.mainEnv.release + 100)
    );

    const leftBox = makeSandbox(
        Math.floor(stereoMode ? maxTime + preset.mainEnv.release + 100 : 0)
    );
    const rightBox = makeSandbox(
        Math.floor(stereoMode ? maxTime + preset.mainEnv.release + 100 : 0)
    );

    const numberOfTones = 128;

    const mono = numberOfTones * MONO_AMMOUNT;

    const tuning = createTopTuning(preset);
    const env = createEnvelope(preset.mainEnv, tuning);
    const pulseProps: PulseProps = {
        env: preset.mainEnv,
        freq: 0,
        partials: tuning.partials,
        tuning,
    };
    LOG('Stereo mode: ', stereoMode);
    const trackCount = allParsedTracks.length;
    allParsedTracks.forEach((pt, i) => {
        console.log(`\nGenerating ${i + 1}. of ${trackCount} tracks.`);
        pt.forEach((n, i) => {
            process.stdout.write(
                `\r${Math.floor(
                    ((i + 1) / pt.length) * 100
                )} %   (Event number ${i + 1} / ${pt.length})`
            );
            const freq = freqPlusCents(tuning.scale[n.note], tuning.baseFreq);

            const { wave: sound } = newComplexTone(
                freq,
                pulseProps.partials,
                n.length,
                env
            );

            const vel = Math.pow(n.velocity / 127, 0.7);

            const volumeSound = multiply(sound, vel);
            const panL = (numberOfTones - n.note + 0.5) / mono;
            const panR = 1 - panL;

            if (!stereoMode) {
                addToSandbox(volumeSound, sbox, Math.floor(n.offset));
            } else {
                addToSandbox(
                    multiply(volumeSound, panL),
                    leftBox,
                    Math.floor(n.offset)
                );
                addToSandbox(
                    multiply(volumeSound, panR),
                    rightBox,
                    Math.floor(n.offset)
                );
            }
        });
    });

    if (!stereoMode) {
        writeTestWave(sbox, outputFileName);
    } else {
        writeStereoWave([leftBox, rightBox], outputFileName);
    }
};

const makeSandbox = (ms: number) => genSilence(ms);

const addToSandbox = (wave: SFG.Wave, sandBox: SFG.Wave, msOffset: number) => {
    const smpOffset = Math.floor(msToSmps(msOffset));

    for (var i = 0; i < wave.length; i++) {
        if (sandBox[i + smpOffset] === undefined) {
            ERROR(
                `undefined regions found while adding wave to sandbox: ${
                    i + smpOffset
                }. End of output wave file could be corupted.`
            );
            break;
        }
        sandBox[i + smpOffset] += wave[i];
    }
};

const writeStereoWave = ([left, right]: SFG.Wave[], filename: string) => {
    LOG('\n\nNormalizing left channel...');
    const normL = normalize(left);
    LOG('Normalizing right channel...');
    const normR = normalize(right);
    const path = `${OUTPUT_DIR}/${OUTPUT_WAVS}`;
    checkAndMakeDir(path);
    console.log('\n\nWriting wave file to disc...');
    writeWaveFile(`${path}/${filename}_stereo.wav`, 2, SR, BR, [normL, normR]);
};

const writeTestWave = (sandbox: SFG.Wave, filename: string) => {
    LOG('\nNormalizing wave...');
    const normalized = normalize(sandbox);
    const path = `${OUTPUT_DIR}/${OUTPUT_WAVS}`;
    checkAndMakeDir(path);
    console.log('\n\nWriting wave file to disc...');
    writeWaveFile(`${path}/${filename}.wav`, 1, SR, BR, normalized);
};

export const parseMidiEvents = (midiPath: string) => {
    const midi60 = loadMidiFile(midiPath);

    console.log('\nHeader of midi file: ', midiPath);
    console.log(midi60.header);
    console.log();

    const { formatType, ticksPerBeat } = midi60.header;
    if (formatType !== 1 && formatType !== 0) {
        console.log(
            '[ERROR]: Not supported MIDI format. Please use MIDI format type 0 or 1.'
        );
        process.exit(0);
    }

    // FIND ONE BEAT LENGTH IN MICROSECONDS
    let microsecondsPerBeat = 0;

    midi60.tracks.forEach((track) => {
        const setTempoEvent = track.find(
            (e) => e.type === 'meta' && e.subtype === 'setTempo'
        );
        if (setTempoEvent) {
            microsecondsPerBeat = (setTempoEvent as SetTempoEvent)
                .microsecondsPerBeat;
            return;
        }
    });
    if (!microsecondsPerBeat) microsecondsPerBeat = 500000;

    const ONE_DELTA =
        (microsecondsPerBeat / ticksPerBeat / 1000) * TEMPO_FACTOR;

    if (TESTING_MODE) {
        //console.log(microsecondsPerBeat / 1000);
        //console.log('one beat length:', microsecondsPerBeat / 1000 / 1000);
        console.log('one delta in ms:', ONE_DELTA);
        console.log('quater note in ms:', ONE_DELTA * ticksPerBeat);
    }

    const parse = (midi: MidiFile) => {
        //console.log('midi.tracks.length');
        //console.log(midi.tracks.length);
        //console.log('------------');
        const parsed: MyMidiTrack[] = midi.tracks.map((t) => {
            let trackLength = 0;
            return t.map((x, i) => {
                trackLength += x.deltaTime;

                if (x.type === 'channel') {
                    if (x.subtype === 'noteOn') {
                        const chEvent = x as NoteOnEvent;
                        return {
                            type: 'ON',
                            note: chEvent.noteNumber as SFG.MidiNote,
                            velocity: chEvent.velocity,
                            deltaTime: chEvent.deltaTime,
                            normalTime: trackLength,
                            index: i,
                        } as MyNoteEvent;
                    }
                    if (x.subtype === 'noteOff') {
                        const chEvent = x as NoteOffEvent;
                        return {
                            type: 'OFF',
                            note: chEvent.noteNumber as SFG.MidiNote,
                            deltaTime: chEvent.deltaTime,
                            normalTime: trackLength,
                            index: i,
                        } as MyNoteEvent;
                    }
                    if (x.subtype === 'controller') {
                        const chEvent = x as ControllerEvent;
                        return {
                            type: 'CC',
                            value: chEvent.value,
                            cc: chEvent.controllerType,
                            deltaTime: chEvent.deltaTime,
                            normalTime: trackLength,
                            index: i,
                        } as MyCCEvent;
                    }
                }
                if (x.type === 'meta') {
                    if (x.subtype === 'endOfTrack') {
                        trackLength += x.deltaTime;
                        return {
                            type: 'META',

                            totalTrackTime: trackLength,
                        } as MyMetaEvent;
                    }
                }

                //return { type: x.type, deltaTime: x.deltaTime };
                return {
                    type: 'META',
                    totalTrackTime: undefined,
                } as MyMetaEvent;
            });
        });

        return parsed;
    };

    const parsed60 = parse(midi60);

    const allParsedTracks: ParsedNoteEvents[][] = [];
    let maxTime = 0;

    for (let tIndex = 0; tIndex < parsed60.length; tIndex++) {
        console.log('Parsing track: ', tIndex + 1);
        const EOT = parsed60[tIndex].find(
            (e) => (e as MyMetaEvent).totalTrackTime
        ) as MyMetaEvent;
        const totalTrackTime = (EOT.totalTrackTime ?? 0) * ONE_DELTA;

        const notesEvents: MyNoteEvent[] = [];

        parsed60[tIndex].forEach((p, i) => {
            if (p.type === 'ON') notesEvents.push(p);
        });
        const parsedNoteEvents = notesEvents.map((p, i) =>
            convertToParsedNoteEvents(p, i + 1, parsed60[tIndex], ONE_DELTA)
        );
        allParsedTracks[tIndex] = parsedNoteEvents;
        maxTime = Math.max(maxTime, totalTrackTime);
    }

    return { allParsedTracks, maxTime };
};

const convertToParsedNoteEvents = (
    ne: MyNoteEvent,
    index: number,
    track: MyMidiTrack,
    oneDelta: number
): ParsedNoteEvents => {
    do {
        //    console.log(index);
        if (track[index].type === 'META' || track[index].type === 'CC') {
            index++;
            continue;
        }

        const event = track[index] as MyNoteEvent;

        if (event.type !== 'OFF' || event.index <= ne.index) {
            index++;
            continue;
        }

        if (event.note === ne.note) {
            return {
                note: ne.note,
                velocity: ne.velocity,
                offset: ne.normalTime * oneDelta,
                length:
                    ((event?.normalTime ?? 0) - (ne?.normalTime ?? 0)) *
                    oneDelta,
            };
        }
        index++;
    } while (index < track.length);
    return { note: -1, velocity: 0, offset: 0, length: 0 };
};

type MyNoteEvent = {
    type: 'ON' | 'OFF';
    note: SFG.MidiNote;
    velocity: number;
    deltaTime: number;
    normalTime: number;
    index: number;
};

type MyMetaEvent = {
    type: 'META';
    totalTrackTime?: number;
};

type MyCCEvent = {
    type: 'CC';
    value: number;
    cc: number;
    deltaTime: number;
    normalTime: number;
    index: number;
};

type MyMidiTrack = (MyNoteEvent | MyMetaEvent | MyCCEvent)[];

type ParsedNoteEvents = {
    note: number;
    velocity: number;
    offset: number; // in seconsts
    length: number; // in seconsts
};
