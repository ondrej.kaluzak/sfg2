import { freqPlusCents } from '../midi/noteToFreq';
import { SFG } from '../types/endpoints';
import { MAX_ALLOWED_DECAY_TIME, SILENCE_dB } from '../utils/envConsts';
import { factorToDb, msToSmps, WARNING } from '../utils/utils';
import { raisedSinEnv } from './basicGenerators';

export const createEnvelope = (
    env: SFG.Envelope,
    tuning: SFG.TopTuning,
    sfz?: boolean
): SFG.Envelope => {
    console.log('Creating envelope ...');

    const { baseFreq, scale } = tuning;

    const commonDecayFreq = freqPlusCents(
        scale[env.decay.commonDecayNote],
        baseFreq
    );

    const dbSpeedPerSec = (1000 * env.decay.dbPerTimeUnit) / env.decay.timeUnit;
    const maxDecayLength = Math.min(
        env.decay.maxDecayLength,
        MAX_ALLOWED_DECAY_TIME
    );
    const decay: SFG.DecayEnv = {
        ...env.decay,
        commonDecayFreq,
        dbSpeedPerSec,
        maxDecayLength,
    };
    const isSustained = factorToDb(env.sustain) > SILENCE_dB;

    const data: SFG.EnvData = {
        attackSmp: msToSmps(env.attack),
        releaseSmp: msToSmps(env.release),
        sustainDb: factorToDb(env.sustain),
        maxDecayLengthSmp: msToSmps(maxDecayLength),
        sfzSus: {
            loopLengthSmp: msToSmps(env.sfzSus.loopLengthMs),
            crossFadeLengthSmp: msToSmps(env.sfzSus.crossFadeMs),
            crossFadeInWave: raisedSinEnv(env.sfzSus.crossFadeMs),
            crossFadeOutWave: raisedSinEnv(env.sfzSus.crossFadeMs).reverse(),
        },
        isSustained,
    };
    let mode: SFG.EnvMode = data.isSustained ? 'ADSR' : 'PERC';
    if (!isSustained && dbSpeedPerSec === 0 && sfz) {
        WARNING(
            'Preset contains zero sustain along with zero decay attenuation.'
        );
        WARNING('This would cause infinite tones in not sustained SFZ mode.');
        WARNING('Program will apply 60s maximum note length rule.');
    }

    if (sfz && mode === 'ADSR') {
        mode = 'SFZLOOP';
    }
    console.log('Envelope mode:', mode);
    return { ...env, decay, mode, data };
};
