"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.genLinSweep = exports.genExpSweep = exports.genWave = void 0;
const SR = 500;
const PI2 = Math.PI * 2;
const genWave = (freq, ms, amp) => {
    const count = Math.floor(SR * (ms / 1000));
    const period = SR / freq;
    let wave = [];
    const a = amp !== null && amp !== void 0 ? amp : 1;
    for (var i = 0; i < count; i++) {
        wave.push(Math.sin((i * PI2) / period) * a);
    }
    return wave;
};
exports.genWave = genWave;
const genExpSweep = (freqStart, freqEnd, ms, amp) => {
    const count = Math.floor(SR * (ms / 1000));
    const step = Math.pow(freqEnd / freqStart, 1 / count);
    let wave = [];
    let f = freqStart;
    const a = amp !== null && amp !== void 0 ? amp : 1;
    for (var i = 0; i < count; i++) {
        const period = SR / f;
        wave.push(Math.sin((i * PI2) / period) * a);
        f = f * step;
    }
    return wave;
};
exports.genExpSweep = genExpSweep;
const genLinSweep = (freqStart, freqEnd, ms, amp) => {
    const count = Math.floor(SR * (ms / 1000));
    const step = (freqStart - freqEnd) / count;
    let wave = [];
    const a = amp !== null && amp !== void 0 ? amp : 1;
    for (var i = 0; i < count; i++) {
        const period = SR / (freqStart + i * step);
        wave.push(Math.sin((i * PI2) / period) * a);
    }
    return wave;
};
exports.genLinSweep = genLinSweep;
