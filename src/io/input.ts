import readlineSync from 'readline-sync';

export const userInput = (message: string) => {
    return readlineSync.question(message);
};

export const userNumber = (message: string) => {
    let ans: undefined | number = undefined;
    while (ans !== 0 && !ans) {
        ans = Number.parseFloat(readlineSync.question(message));
        if (ans !== 0 && !ans) console.log('Invalid number, try again...\n');
    }
    return ans
}
