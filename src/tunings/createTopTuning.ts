import { noteToFreq } from '../midi/noteToFreq';
import { getPartialsTable } from '../tunings/partialsTable';
import { genTopTuning } from '../tunings/topScale';
import { SFG } from '../types/endpoints';

export const createTopTuning = (preset: SFG.Preset): SFG.TopTuning => {
    const { tuningParams } = preset;
    const { count, attenPower } = preset.partialsSettings;

    const baseFreq = noteToFreq(tuningParams.centerNote); //TODO: center note shift?

    console.log('Computing primes and scale...');

    const { primes, scale, scalaFormat, steps } = genTopTuning(tuningParams);

    console.log('Generating partials table...');
    const partials = getPartialsTable(primes, count, attenPower);

    return { scale, partials, baseFreq, steps, scalaFormat };
};

// const scaleWaves = [...Array(finalCount).keys()].map((i) => {
//     const freq = freqPlusCents(scale[i + finalOffset], baseFreq);

//     console.log(
//         `${i + 1}/${finalCount}: generating note ${
//             i + finalOffset
//         } with freq: ${freq}`
//     );
//     return normalize(tunedPulse(freq, partials, envMs, decayPower));
// });
