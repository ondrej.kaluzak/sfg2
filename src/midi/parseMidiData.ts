import { loadMidiFile } from './loadMidiFile';
import { MidiFile, NoteOnEvent, read } from 'midifile-ts';

type TNote = {
    freq: number;
    amp: number;
    ms: number;
};

// const notes: TNote[] = [{ freq: 440, volume: 0.4, length: 1000 },
// { freq: 230, volume: 0.7, length: 5000 },
// { freq: 812, volume: 0.1, length: 1000 },
// { freq: 100, volume: 0.7, length: 6000 }];

// const track1: TNote[] = [...Array(50).keys()].map(_ => {
//     const freq = getRandomInt(800) + 20;
//     const amp = (getRandomInt(100) + 1) / 100;
//     const ms = getRandomInt(2000) + 50;
//     return ({ freq, amp, ms });
// });

// const track2: TNote[] = [...Array(50).keys()].map(_ => {
//     const freq = getRandomInt(800) + 20;
//     const amp = (getRandomInt(100) + 1) / 100;
//     const ms = getRandomInt(2000) + 50;
//     return ({ freq, amp, ms });
// });

// const wx = connect(track1.map(n => genPulse(n.freq, n.ms, n.amp)));
// const wy = connect(track2.map(n => genPulse(n.freq, n.ms, n.amp)));

// const w1 = [...wx, ...amWaves([wx,wy])];
//const w2 = [...wy, ...amWaves([wx,wy])];

//const song = sumWaves([w1, w2]);

//console.log(song);

//const w = genWave(440,5000);
//const w = genExpSweep(50, 1050, 100);
//const w2 = genLinSweep(50, 1050, 100);
//const res = normalize(connect([w, w2]));

//const midi = loadMidiFile('./binary/example.mid');
const midi60 = loadMidiFile('./binary/example60.mid');

//const res = normalize(song);

//console.log(midi.header);
//console.log(midi60.header);
//console.log(midi60.tracks);
// const parse = (midi: MidiFile) => {
//     const parsed = midi.tracks
//         .map((t) =>
//             t.map((x) => {
//                 if (x.type === 'channel') {
//                     if (x.subtype === 'noteOn') {
//                         const chEvent = x as NoteOnEvent;
//                         return {
//                             type: chEvent.subtype,
//                             note: chEvent.noteNumber,
//                             deltaTime: chEvent.deltaTime,
//                         };
//                     }
//                 }
//                 return { type: 'skip', deltaTime: x.deltaTime };
//             })
//         )
//         .flat();
//     return parsed;
// };

//const parsed60 = parse(midi60);

//const songLength60 = parsed60.reduce((acc, p) => acc + p.deltaTime, 0);
//console.log(songLength60);

//console.log(freqPlusCents(-1200, 440));

// const song = connect(
//     parsed60.map((p) => {
//         if (p.type === 'noteOn') {
//             const soundProps: SFG.SoundProps = {
//                 amp: 1, freq: noteToFreq(p.note ?? 0), ms: 1000,
//             }

//             return tunedPulse(soundProps, partials, env);
//         } else return genSilence(2000);
//     })
//);
// console.log(parsed);
