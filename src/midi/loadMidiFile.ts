import fs from 'fs';
import { read } from 'midifile-ts';

export const loadMidiFile = (path: string) => {
    try {
        const buffer = fs.readFileSync(path);
        const ab = toArrayBuffer(buffer);
        return read(ab);
    } catch (error) {
        console.log('\nUnable to load midi file ', path);
        console.log();
        process.exit(0);
    }
};

function toArrayBuffer(buf: Buffer) {
    var ab = new ArrayBuffer(buf.length);
    var view = new Uint8Array(ab);
    for (var i = 0; i < buf.length; ++i) {
        view[i] = buf[i];
    }
    return ab;
}
