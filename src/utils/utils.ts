import { CONSOLE_USE_COLORS, SR } from './envConsts';
import fs from 'fs';
export const msToSmps = (ms: number): number => Math.floor((SR * ms) / 1000);
export const smpsToMs = (smps: number): number =>
    Math.floor((smps / SR) * 1000);

const centsToFactor = (c: number) => Math.pow(2, c / 1200);

export const dbToFactor = (db: number) => Math.pow(10, db / 20);
export const factorToDb = (f: number) => 20 * Math.log10(f);

export const sanitizeName = (name: string) =>
    name.replaceAll('\n', '').replaceAll('\r', '');

export const isNan = (n: unknown) => Number.isNaN(n);
export const isInt = (n: unknown) => Number.isInteger(n);

const getRandomInt = function (maximum: number): number {
    return Math.floor(Math.random() * Math.floor(maximum));
};

export function findGcd(arr: number[]) {
    // Use spread syntax to get minimum of array

    arr = arr.map((a) => Math.abs(a));
    const lowest = Math.min(...arr);
    const num = arr.length;

    for (let factor = lowest; factor > 1; factor--) {
        let isCommonDivisor = true;

        for (let j = 0; j < num; j++) {
            if (arr[j] % factor !== 0) {
                isCommonDivisor = false;
                break;
            }
        }

        if (isCommonDivisor) {
            return factor;
        }
    }

    return 1;
}

export function checkAndMakeDir(path: string) {
    if (!fs.existsSync(path)) {
        fs.mkdirSync(path);
    }
}

export function LOG(...msg: any[]) {
    console.log(...msg);
}

export function INFO(msg: string) {
    const color = CONSOLE_USE_COLORS ? '\x1b[32m%s\x1b[0m' : '';
    console.log(color, '[INFO] ', msg);
}

export function WARNING(msg: string) {
    const color = CONSOLE_USE_COLORS ? '\x1b[36m%s\x1b[0m' : '';
    console.log(color, '[WARNING] ', msg);
}

export function ERROR(msg: string) {
    const color = CONSOLE_USE_COLORS ? '\x1b[31m%s\x1b[0m' : '';
    console.log(color, '[ERROR] ', msg);
}
