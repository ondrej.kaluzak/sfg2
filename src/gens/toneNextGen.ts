import { SFG } from '../types/endpoints';
import {
    MAX_FREQ_TO_RENDER,
    PI2,
    SILENCE_dB,
    SILENCE_FACTOR,
    SR,
} from '../utils/envConsts';
import { dbToFactor, factorToDb, msToSmps } from '../utils/utils';
import {
    addToWave,
    amWith,
    amWithSmp,
    copyFromWave,
    gen1SMP,
    softenEdges,
    softenStart,
    sumWaves,
} from './basicGenerators';

export interface PulseProps {
    freq: number;
    tuning: SFG.TopTuning;
    partials: SFG.Partials;
    env: SFG.Envelope;
}

export const newComplexTone = (
    freq: number,
    partials: SFG.Partials,
    noteLength: number,
    env: SFG.Envelope
) => {
    //console.log(env);

    const { commonDecayFreq, speedPower, dbSpeedPerSec } = env.decay;
    const { sustainDb, isSustained, maxDecayLengthSmp } = env.data;

    const dbPerSecFundament =
        dbSpeedPerSec * (freq / commonDecayFreq) ** speedPower;

    const dbSpeedPerSampel = dbPerSecFundament / SR;

    const fundamentDecayPartLengthSmp = Math.floor(
        Math.abs(sustainDb / dbSpeedPerSampel)
    );
    let noteLengthFinalSmp = msToSmps(noteLength);

    const { attackSmp } = env.data;
    const { crossFadeLengthSmp, loopLengthSmp } = env.data.sfzSus;

    if (env.mode === 'SFZLOOP') {
        noteLengthFinalSmp =
            Math.min(fundamentDecayPartLengthSmp, maxDecayLengthSmp) +
            loopLengthSmp +
            attackSmp +
            crossFadeLengthSmp;
    }

    const noteLengthSmp =
        isSustained || env.mode === 'PERC'
            ? noteLengthFinalSmp
            : Math.min(fundamentDecayPartLengthSmp, maxDecayLengthSmp);

    const wave = sumWaves(
        [...Array(partials.factors.length)].map((_, i) => {
            if (
                partials.amps[i] < SILENCE_FACTOR ||
                freq * partials.factors[i] > MAX_FREQ_TO_RENDER
            ) {
                return gen1SMP(0);
            }
            const partialFreq = freq * partials.factors[i];
            const dbPerSec =
                dbSpeedPerSec * (partialFreq / commonDecayFreq) ** speedPower;

            return genWaveNext(
                partialFreq,
                dbPerSec,
                fundamentDecayPartLengthSmp,
                partials.amps[i],
                noteLengthSmp,
                env
            );
        })
    );

    if (env.mode === 'SFZLOOP') {
        const crossFadeIn = env.data.sfzSus.crossFadeInWave;
        const crossFadeOut = env.data.sfzSus.crossFadeOutWave;

        const susPointOffsetSmp =
            Math.min(fundamentDecayPartLengthSmp, maxDecayLengthSmp) +
            attackSmp;

        const crossInWave = copyFromWave(
            wave,
            susPointOffsetSmp,
            crossFadeLengthSmp
        );

        /*  const releaesPart = copyFromWave(
            wave,
            susPointOffsetSmp + crossFadeLengthSmp,
            releasePartLengthSmp
        ); */

        amWithSmp(wave, crossFadeOut, wave.length - crossFadeOut.length);
        amWith(crossInWave, crossFadeIn, 0);

        addToWave(wave, crossInWave, noteLengthSmp - crossFadeLengthSmp + 2);

        return {
            wave: softenStart(wave, 2), // softenEdges(connect([wave, releaesPart]), 1, 1),
            loopStartIndex: susPointOffsetSmp + crossFadeLengthSmp - 1,
            loopEndIndex: wave.length - 1, // index ktory je posledny pred pridanim release
        };
    } else
        return {
            wave: softenEdges(wave, 2, 15),
            loopStartIndex: 0,
            loopEndIndex: 0,
        };
};

//const cyclePhase = (phase:number, freq)=>
class PhaseCycle {
    phase = 0;
    freq = 0;
    constructor(freq: number) {
        this.freq = freq;
    }
    next() {
        this.phase += (PI2 * this.freq) / SR;
        return Math.sin(this.phase);
    }
}

export const genWaveNext = (
    freq: number,
    dbSpeedPerSec: number,
    fundamentDecayPartLengthSmp: number,
    partialAmp: number,
    noteLengthSmp: number,
    env: SFG.Envelope
): SFG.Wave => {
    let p: PhaseCycle = new PhaseCycle(freq);

    const afterAttackLevel = factorToDb(partialAmp);
    const dbSpeedPerSampel = dbSpeedPerSec / SR;
    const decayWave: SFG.Wave = [0];
    // pre vyssie tony je start position hlasnejsia
    const attackStartLevel =
        SILENCE_dB +
        factorToDb(freq / MAX_FREQ_TO_RENDER) +
        Math.abs(SILENCE_dB / 4);

    const attackSmpsCount = msToSmps(env.attack);
    let amp = 1;
    let ampDb = attackStartLevel;

    if (env.attack) {
        //vypocitam ako rychlo mam zhlasnovat aby som dosiahol after attack hladinu
        const attacktDbSpeedPerSampel =
            Math.abs(Math.abs(afterAttackLevel) - Math.abs(attackStartLevel)) /
            attackSmpsCount;
        for (let i = 0; i < attackSmpsCount; i++) {
            if (i > noteLengthSmp) break;
            ampDb += attacktDbSpeedPerSampel;
            amp = dbToFactor(ampDb);
            decayWave.push(p.next() * amp);
        }
    }
    amp = 1;
    //ampDb = afterAttackLevel;

    for (let i = 0; i < noteLengthSmp; i++) {
        //ak som pod hranicou ticha
        if (ampDb < SILENCE_dB) break;
        if (i + attackSmpsCount > noteLengthSmp) break;
        //ak som uz dosiahol sus point pri fundamente
        if (i >= fundamentDecayPartLengthSmp) {
            decayWave.push(p.next() * amp);
        } else {
            ampDb -= dbSpeedPerSampel;
            amp = dbToFactor(ampDb);

            decayWave.push(p.next() * amp);
        }
    }

    {
        /**RELEASE pri ADSR*/
    }
    if (env.mode === 'ADSR') {
        const releaseSmpsCount = msToSmps(env.release);
        //vypocitam ako rychlo mam stisovat aby som dosiahol ticho za dlzku release
        const releaseDbSpeedPerSampel =
            Math.abs(Math.abs(SILENCE_dB) - Math.abs(ampDb)) / releaseSmpsCount;
        for (let i = 0; i < releaseSmpsCount; i++) {
            ampDb -= releaseDbSpeedPerSampel;
            amp = dbToFactor(ampDb);

            decayWave.push(p.next() * amp);
        }
    }

    return decayWave;
};
