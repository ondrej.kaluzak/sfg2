superpyth
[ 20480, 19683 ]  y-x = gen
[ 12, -9, 1 ]
1
[ 1197.5961209563397, 1905.7650590058583, 2780.7320795766486 ]
Generator:  322.62910038554946
factors: [ 1.9972248571681341, 3.006609598098882, 4.983905566841072 ]
{
  coordsObj: { '2': 12, '3': -9, '5': 1 },
  sortedKeys: [ '2', '3', '5' ],
  numberVector: [ 12, -9, 1 ],
  multival: [ 1, 9, 12 ]
} 


[ 81, 80 ] => 2*x - y = gen (alebo aj y-x)
[ -4, 4, -1 ]
-1
[ 1201.698520494566, 1899.2629095747936, 2790.2575563209098 ]
Generator:  310.70387374844995
factors: [ 2.0019631706996375, 2.9953385864986664, 5.011403247330422 ]
{
  coordsObj: { '2': -4, '3': 4, '5': -1 },
  sortedKeys: [ '2', '3', '5' ],
  numberVector: [ -4, 4, -1 ],
  multival: [ 1, 4, 4 ]
}

helmholtz
[ 32805, 32768 ] => gen = y-x = 701.79
[ -15, 8, 1 ]
-1
[ 1200.065120492174, 1901.851787327263, 2786.162508764503 ]
Generator:  315.7543990549341
factors: [ 2.00007523155749, 2.9998211498990726, 4.999563321615822 ]
{
  coordsObj: { '2': -15, '3': 8, '5': 1 },
  sortedKeys: [ '2', '3', '5' ],
  numberVector: [ -15, 8, 1 ],
  multival: [ 1, 8, 15 ]
}

[ 20000, 19683 ] => gen = (z - y) / 5
[ 5, -9, 4 ]
1
[ 1199.0312592987011, 1903.4904185498685, 2784.0643676138275 ]
Generator:  318.4573102347422
factors: [ 1.998881179913355, 3.0026618563125482, 4.993507851734261 ]
{
  coordsObj: { '2': 5, '3': -9, '5': 4 },
  sortedKeys: [ '2', '3', '5' ],
  numberVector: [ 5, -9, 4 ],
  multival: [ 4, 9, 5 ]
}
Hanson
gen = 317
[ 15625, 15552 ]
[ -6, -5, 6 ]
-1
[ 1200.2910384820213, 1902.4162859456578, 2785.637943436736 ]
Generator:  317.0693809909433
factors: [ 2.000336249101443, 3.0007994526338915, 4.998048679335402 ]
{
  coordsObj: { '2': -6, '3': -5, '5': 6 },
  sortedKeys: [ '2', '3', '5' ],
  numberVector: [ -6, -5, 6 ],
  multival: [ 6, 5, 6 ]
}

5x + 2y = 1200.0651
5x = 1200.0651-2y
1903


Dobrý deň,

dnes som sa chvíľu venoval programu a myslím, že sa mi podarilo decay prerobiť podla požiadaviek ktoré ste mi popísali. 
Čiže, budú sa brať nové parametre ako rýchlosť zoslabovania parciál v decibeloch za nejakú časovú jednotku. Narazil som ale aj na jeden problém.


Ak sa frekvencia, ktorá je o oktávu vyššie než Common decay frekvencia majá utlmovať 2x rýchlejšie, 
tak predpokladám, že frekvencia, ktorá je o oktávu nižšie zase 2x pomalšie. A tu vzniká problém, že ak nemám dopredu vôbec defi


tak čo presne sa má diať pri frekvenciách ktoré sú napríklad o oktávu, alebo dve či tri oktávy nižšie než Common decay frekvencia? 
Lebo ak sa nastaví, že napríklad frekvencia 440Hz sa utlmuje 6dB/s, tak 55Hz sa bude utlmovať 0.75dB / s, čo je dosť pomaly. A problém samozrejme je, že sa budú musieť pre nízke frekvencie generovať pomerne dlhé wav súbory, aby tón stihol nejak doznieť a prekročiť hranicu, keď ho budem už považovať za utlmený.


Dajme tomu, že užívateľ navolí že frekvencia rovná MIDI note 60 sa stišuje rýchlosťou 6dB za sekundu. Tým pádom  nota o oktávu vyššie, alebo parciálny tón ktorý má dvojnásobnú frekvenciu sa bude stišovat 2x rýchlejšie, čiže 12dB za sekundu.. a tak ďalej. Tam problém nie je, lebo čím vyššia frekvencia, tým sa len tón rýchlejšie utlmí a stačí vygenerovať kratšiu vlnu.

Kde môže vzniknúť ale nezrovnalosť je, keď ideme smerom dole. Pri tomto nastavení by sa napr MIDI nota č.24 mala stišovať 0.75 dB za sekundu, pretože predpokladám, že pravidlo dvojnásobnej rýchlosti sa uplatňje aj na opačn