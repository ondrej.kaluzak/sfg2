import { noteToFreq } from '../midi/noteToFreq';
import { writeScalaFile } from '../scala/writeScala';
import { SFG } from '../types/endpoints';
import { sanitizeName } from '../utils/utils';
import { getPartialsTable } from './partialsTable';
import { genTopTuning } from './topScale';

const createUsableScale = (preset: SFG.Preset) => {
    const { name: rawName, tuningParams } = preset;

    const { count, attenPower } = preset.partialsSettings;

    const baseFreq = noteToFreq(tuningParams.centerNote);

    console.log('Computing primes and scale...');

    const { primes, scale, scalaFormat } = genTopTuning(tuningParams);

    console.log('Generating partials table...');
    const partials = getPartialsTable(primes, count, attenPower);

    //  const envMs = makeEnv(attack, decay, release, attackSteep, releaseSteep);
    return { primes, scale, scalaFormat, baseFreq, partials };
};
