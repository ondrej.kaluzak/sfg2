/* tslint:disable */
/* eslint-disable */
export namespace SFG {
    type Sampel = number;
    type PartialFactor = number;
    type PartialAmp = number;

    type EnvMode = 'SFZLOOP' | 'ADSR' | 'PERC';

    interface Partials {
        amps: PartialAmp[];
        factors: PartialFactor[];
    }

    type Wave = Sampel[];

    interface SoundProps {
        freq: number;
        amp: number;
        ms: number;
        brightness?: number;
    }

    type DecayEnv = {
        dbPerTimeUnit: number;
        timeUnit: number;
        dbSpeedPerSec: number;
        maxDecayLength: number;
        commonDecayNote: number;
        commonDecayFreq: number;
        speedPower: number;
    };

    type EnvData = {
        attackSmp: number;
        sustainDb: number;
        releaseSmp: number;
        maxDecayLengthSmp: number;
        sfzSus: {
            loopLengthSmp: number;
            crossFadeLengthSmp: number;
            crossFadeInWave: SFG.Wave;
            crossFadeOutWave: SFG.Wave;
        };
        isSustained: boolean;
    };

    type Envelope = {
        attack: number;
        sustain: number;
        release: number;
        mode: EnvMode;
        decay: DecayEnv;
        sfzSus: {
            loopLengthMs: number;
            crossFadeMs: number;
        };
        data: EnvData;
    };

    type Vector = [x: number, y: number, z: number];

    type Tuning = {
        primes: Vector;
        scale: number[];
        scalaFormat: number[];
        steps?: number[];
        scaleOnePeriod?: number[];
    };

    type Comma = [m: number, n: number];
    const MakeTuple = <T extends number[]>(...args: T) => args;

    type TuningParams = {
        commaToTemper: Comma;
        periodLength: number;
        negativeGenerators: number;
        centerNote: MidiNote;
        centerNoteShift: number;
        notTopTuning?: boolean;
    };

    type PartialsSettings = {
        count: number;
        attenPower: number;
        partialsEnv: SFG.PresetEnvelope[];
    };
    // created top tunning with all important to gen. sound
    type TopTuning = {
        scale: number[];
        partials: SFG.Partials;
        baseFreq: number; // TODO center note shift
        steps?: number[]; //only for testing
        scalaFormat: number[]; //only for testing
    };

    type Preset = {
        name: string;
        tuningParams: TuningParams;
        mainEnv: Envelope;
        partialsSettings: PartialsSettings;
    };

    type SampleRateType = 100 | 1000 | 22500 | 44100 | 48000 | 96000;
    type BitDepthType = '8' | '16' | '24' | '32' | '32f';

    //TS RANGE HELPERS

    type MidiNote =
        | 0
        | 1
        | 2
        | 3
        | 4
        | 5
        | 6
        | 7
        | 8
        | 9
        | 10
        | 11
        | 12
        | 13
        | 14
        | 15
        | 16
        | 17
        | 18
        | 19
        | 20
        | 21
        | 22
        | 23
        | 24
        | 25
        | 26
        | 27
        | 28
        | 29
        | 30
        | 31
        | 32
        | 33
        | 34
        | 35
        | 36
        | 37
        | 38
        | 39
        | 40
        | 41
        | 42
        | 43
        | 44
        | 45
        | 46
        | 47
        | 48
        | 49
        | 50
        | 51
        | 52
        | 53
        | 54
        | 55
        | 56
        | 57
        | 58
        | 59
        | 60
        | 61
        | 62
        | 63
        | 64
        | 65
        | 66
        | 67
        | 68
        | 69
        | 70
        | 71
        | 72
        | 73
        | 74
        | 75
        | 76
        | 77
        | 78
        | 79
        | 80
        | 81
        | 82
        | 83
        | 84
        | 85
        | 86
        | 87
        | 88
        | 89
        | 90
        | 91
        | 92
        | 93
        | 94
        | 95
        | 96
        | 97
        | 98
        | 99
        | 100
        | 101
        | 102
        | 103
        | 104
        | 105
        | 106
        | 107
        | 108
        | 109
        | 110
        | 111
        | 112
        | 113
        | 114
        | 115
        | 116
        | 117
        | 118
        | 119
        | 120
        | 121
        | 122
        | 123
        | 124
        | 125
        | 126
        | 127;
}
//export const SR: SFG.SampleRateType = 22500;
//export const BT: SFG.BitDepthType = "32f";
