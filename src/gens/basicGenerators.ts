import { SFG } from '../types/endpoints';
import { PI2, SR, TESTING_MODE } from '../utils/envConsts';
import { dbToFactor, factorToDb, msToSmps } from '../utils/utils';

const f1 = (x: number, steep?: number) => Math.pow(x, 1 / (steep ?? 3));
const f2 = (x: number, steep?: number) => Math.pow(x, steep ?? 3);
const g1 = (x: number, l: number, u: number) =>
    f1(((x - l) / (u - l)) * (u - l) + l);

// const g2 = (x: number, l: number, u: number, steep?: number) =>
//     f2(((x - l) / (u - l)) * (u - l) + l, steep);

const g2 = (x: number, l: number, u: number, steep?: number) =>
    f2((x - l) / (u - l), steep) * (u - l) + l;

// const fadeOutADSR = (ms: number, steep?: number) => {
//         const count = Math.floor((SR * ms) / 1000);
//         return [...Array(count).keys()].map((i) => {
//             const xa = 0; //zaciatok
//             const xd = count; // koniec - dlzka
//             const ys = 0; // konecny stav (iny ak je tam nejaky sustain point)
//             return g2(((ys - 1) / xd) * (i - xa) + 1, ys, 1);
//         });
//     };

//fade in
export const fadeIn = (ms: number, steep?: number): SFG.Wave => {
    const count = Math.floor((SR * ms) / 1000);
    return [...Array(count).keys()].map((i) => f1(i / count, steep));
};

export const fadeOut = (
    ms: number,
    steep?: number,
    target?: number,
    offset?: number
): SFG.Wave => {
    const count = Math.floor((SR * ms) / 1000);
    const t = target ?? 0;
    const off = offset ?? 1;
    return [...Array(count).keys()].map((i) => {
        return g2(((t - off) / count) * (i + 1) + off, t, off, steep);
    });
};

// export const genAdsrEnvWave = (env: SFG.EnvMs, realNoteLength: number) => {
//     const att = fadeIn(env.attack, env.attackSteep);

//     const decay = fadeOut(env.decay, 3, env.sustain);
//     const sus = addConstant(genSilence(Math.floor(realNoteLength)), 0.2);
//     const release = fadeOut(env.release, env.releaseSteep, 0, 0.2);
//     return connect([att, decay, sus, release]);
// };
// export const genADEnvWave = (env: SFG.EnvMs): SFG.Wave => {
//     const att = fadeIn(env.attack, env.attackSteep);
//     const decay = fadeOut(env.decay, env.decaySteep, env.sustain);
//     return connect([att, decay]);
// };

// export const applyAdsr = (env: SFG.EnvMs, wave: SFG.Wave) => {
//     const att = fadeIn(env.attack, env.attackSteep);
//     const decay = fadeOut(env.decay, 3, env.sustain);
//     // const sus = addConstant(genSilence(Math.floor(realNoteLength)), 0.2);
// };

export const genRamp = (ms: number, steepness?: number): SFG.Wave => {
    const count = Math.floor(SR * (ms / 1000));
    console.log(count);
    return [...Array(count).keys()].map(
        (_, i) => 1 / Math.pow(2, i / (count / (steepness ?? 10)))
    );
};

export const AgenRamp = (ms: number, steepness?: number): SFG.Wave => {
    const count = Math.floor(SR * (ms / 1000));
    return [...Array(count).keys()].map(
        (_, i) => 1 / Math.pow(2, i / (count / (steepness ?? 10)))
    );
};

// export const fadeIn = (ms: number, steepness?: number): SFG.Wave =>
//     genRamp(ms, steepness).reverse();

// export const fadeOut = (ms: number, steepness?: number): SFG.Wave =>
//     genRamp(ms, steepness);

//TODO: fadeout na koniec vlny, vzdy sa bude aplikovat na koniec - parameter asi pocet ms od konca
export const softenEdges = (
    wave: SFG.Wave,
    msFromStart: number,
    msFromEnd: number
): SFG.Wave => {
    wave = amWaves([fadeIn(msFromStart, 3), wave]);
    const fo = fadeOut(msFromEnd, 3);
    const smps = msToSmps(msFromEnd);
    for (var i = 0; i < smps; i++) {
        wave[i + wave.length - smps] *= fo[i];
    }
    return wave;
};

export const softenEnd = (
    wave: SFG.Wave,
    msFromEnd: number,
    releaseSteep?: number
): SFG.Wave => {
    const fo = fadeOut(msFromEnd, releaseSteep ?? 3, 0);
    const smps = msToSmps(msFromEnd);
    for (var i = 0; i < smps; i++) {
        wave[i + wave.length - smps] *= fo[i];
    }
    return wave;
};

export const softenStart = (wave: SFG.Wave, msFromStart: number): SFG.Wave => {
    const fi = fadeIn(msFromStart, 3);
    const smps = msToSmps(msFromStart);
    for (var i = 0; i < smps; i++) {
        wave[i] *= fi[i];
    }
    return wave;
};

export const raisedSinEnv = (ms: number): SFG.Wave => {
    const count = Math.floor((SR * ms) / 1000);
    var out = [];
    for (var i = 0; i < count; i++) {
        out.push(Math.sin((i * PI2) / (count * 4)) ** 2);
    }
    return out;
};

export const genWave = (freq: number, ms: number, amp?: number): SFG.Wave => {
    if (freq > SR / 2 || freq < 5) {
        return genSilence(ms);
    }
    const count = Math.floor(SR * (ms / 1000));
    const period = SR / freq;
    let wave: SFG.Wave = [];
    const a = amp === undefined ? 0 : amp;
    for (var i = 0; i < count; i++) {
        wave.push(Math.sin((i * PI2) / period) * a);
    }
    return wave;
};

export const genSilence = (ms: number, val?: number): SFG.Wave => {
    const count = Math.floor(SR * (ms / 1000));
    return [...Array(count).keys()].map((_) => (val === undefined ? 0 : val));
};

const interpolate = (x: number, y: number, steps: number) => {
    let temp: number[] = new Array(steps);
    steps--;
    const onestep = (y - x) / steps;
    temp[0] = x;
    steps--;
    while (steps >= 0) {
        temp[temp.length - steps - 1] = temp[temp.length - steps - 2] + onestep;
        steps--;
    }
    return temp;
};

export const genNoize = (
    smpsCount: number,
    amp: number = 1,
    lowPassNum: number = 0
): SFG.Wave => {
    const wave: SFG.Wave = new Array(smpsCount);
    wave[0] = 0;
    wave[wave.length - 1] = 0;

    for (let i = 1; i < wave.length - 1; i++) {
        wave[i] = (Math.random() * 2 - 1) * amp;
        for (let lp = 0; lp < lowPassNum; lp++) {
            wave[i] = (wave[i - 1] + wave[i]) / 2;
        }
    }
    /*  for (let i = 0; i < lowPassNum; i++) {
        wave.forEach((w, index) => {
            if (index > 0) {
                wave[index] = (wave[index - 1] + w) / 2;
            }
        });
    } */

    return wave;
};

export const gen1SMP = (val: number): SFG.Wave => [val];

export const genExpSweep = (
    freqStart: number,
    freqEnd: number,
    ms: number,
    amp?: number
): SFG.Wave => {
    const count = Math.floor(SR * (ms / 1000));
    const step = Math.pow(freqEnd / freqStart, 1 / count);
    let wave: SFG.Wave = [];
    let f = freqStart;
    const a = amp ?? 1;
    for (var i = 0; i < count; i++) {
        const period = SR / f;
        wave.push(Math.sin((i * PI2) / period) * a);
        f = f * step;
    }
    return wave;
};

export const genLinSweep = (
    freqStart: number,
    freqEnd: number,
    ms: number,
    amp?: number
): SFG.Wave => {
    const count = Math.floor(SR * (ms / 1000));
    const step = (freqStart - freqEnd) / count;
    let wave: SFG.Wave = [];

    const a = amp ?? 1;
    for (var i = 0; i < count; i++) {
        const period = SR / (freqStart + i * step);
        wave.push(Math.sin((i * PI2) / period) * a);
    }
    return wave;
};

export const genPulse = (freq: number, ms: number, amp?: number): SFG.Wave => {
    const wave = genTri(freq, ms, amp);
    const decay = genRamp(ms);
    const fi = fadeIn(ms / 5);
    return amWaves([wave, decay, fi]);
};

const genTri = (freq: number, ms: number, amp?: number) =>
    multiply(
        sumWaves(
            [...Array(10)].map((_, i) =>
                multiply(genWave(freq * (i + 1), ms), 1 / Math.pow(1.1, i))
            )
        ),
        amp ?? 1
    );

export const amWaves = (waves: SFG.Wave[]) => {
    const longest = Math.max(...waves.map((w) => w.length));
    const amWave: SFG.Wave = [];
    for (var i = 0; i < longest; i++) {
        amWave.push(
            waves.reduce((acc, w) => acc * (w[i] === undefined ? 1 : w[i]), 1)
        );
    }

    return amWave;
};

export const addToWave = (
    wave: SFG.Wave,
    toAddWave: SFG.Wave,
    smpOffset: number
    // msOffset: number
) => {
    // const smpOffset = Math.floor(msToSmps(msOffset));
    //console.log(smpOffset);
    for (var i = 0; i < toAddWave.length; i++) {
        if ([i + smpOffset] === undefined) console.log('[ERROR]: out of range');
        wave[i + smpOffset] = wave[i + smpOffset] + toAddWave[i];
    }
};

export const copyFromWave = (
    wave: SFG.Wave,
    smpOffset: number,
    smpLength: number
): SFG.Wave => {
    //   const smpOffset = Math.floor(msToSmps(msOffset));
    //   const smpLength = Math.floor(msToSmps(msLength));
    const newWave = [];
    //console.log(smpOffset);
    for (var i = smpOffset; i < smpOffset + smpLength; i++) {
        if (wave[i] === undefined) return newWave;
        newWave.push(wave[i]);
    }
    return newWave;
};

export const amWith = (
    waveSoruce: SFG.Wave,
    waveMod: SFG.Wave,
    startMs: number
) => {
    // if (waveSoruce.length < waveMod.length) {
    //     if (TESTING_MODE) {
    //         console.log(
    //             '\n[ERROR]: waveMod is longer than waveSource in amWith. Returning waveSource.\n'
    //         );
    //     }
    //     return waveSoruce;
    // }

    const startSmp = Math.floor(msToSmps(startMs));

    for (var i = 0; i < waveMod.length; i++) {
        if (i + startSmp >= waveSoruce.length) break;
        waveSoruce[i + startSmp] *= waveMod[i];
    }

    return waveSoruce;
};

export const amWithSmp = (
    waveSoruce: SFG.Wave,
    waveMod: SFG.Wave,
    startSmp: number
) => {
    for (var i = 0; i < waveMod.length; i++) {
        if (i + startSmp >= waveSoruce.length) break;
        waveSoruce[i + startSmp] *= waveMod[i];
    }

    return waveSoruce;
};

// vysledna dlzka je tej ktora je najkratsia
export const amWavesStrict = (waves: SFG.Wave[]): SFG.Wave =>
    [...Array(Math.min(...waves.map((w) => w.length))).keys()].map((i) =>
        waves.reduce((acc, w) => acc * (w[i] === undefined ? 1 : w[i]), 1)
    );

export const multiply = (wave: SFG.Wave, constant: number): SFG.Wave =>
    constant === 1 ? wave : wave.map((s) => s * constant);

export const addConstant = (wave: SFG.Wave, constant: number): SFG.Wave =>
    constant === 0 ? wave : wave.map((s) => s + constant);

export const sumWaves = (waves: SFG.Wave[]) => {
    const longest = Math.max(...waves.map((w) => w.length));
    const waveSum: SFG.Wave = [];
    for (var i = 0; i < longest; i++)
        waveSum.push(
            waves.reduce((acc, w) => acc + (w[i] === undefined ? 0 : w[i]), 0)
        );

    return waveSum;
};

export const normalize = (wave: SFG.Wave): SFG.Wave => {
    const length = wave.length;
    if (length < 1) return [];
    const { max, min } = getPeaks(wave);
    const c = (1 / Math.max(Math.abs(max), Math.abs(min))) * 0.99;
    return [...wave.map((s) => s * c)];
};

type MS = number;
// export const makeEnv = (
//     attack: MS,
//     decay: MS,
//     release: MS,
//     attackSteep?: number,
//     releaseSteep?: number
// ): SFG.EnvMs => ({
//     attack,
//     decay,
//     release,
//     attackSteep,
//     releaseSteep,
//     length: attack + decay + release,
// });
// prakticky je to tiez len AD pri perkusivnych nastrojoch
// export const genWaveEnvMs = (env: SFG.Envelope): SFG.Wave =>
//     connect([
//         fadeIn(env.attack, env.attackSteep),
//         fadeOut(env.decay, env.decaySteep, 0, 1),
//     ]);

export const connect = (waves: SFG.Wave[]): SFG.Wave => [...waves.flat()];

export const delayWave = (wave: SFG.Wave, ms: number) =>
    connect([genSilence(ms), wave]);

export function getPeaks(wave: SFG.Wave): { min: number; max: number } {
    let len = wave.length;
    let max = -Infinity;
    let min = Infinity;

    while (len--) {
        max = wave[len] > max ? wave[len] : max;
        min = wave[len] < min ? wave[len] : min;
    }
    return { max, min };
}

export const invertWave = (wave: SFG.Wave) => wave.map((s) => s * -1);
