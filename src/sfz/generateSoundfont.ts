import { normalize } from '../gens/basicGenerators';
import { freqPlusCents, noteToFreq } from '../midi/noteToFreq';
import { writeScalaFile } from '../scala/writeScala';
import { SFG } from '../types/endpoints';
import { writeSfz } from './writeSfzFile';
import fs from 'fs';
import {
    MAX_FREQ_TO_RENDER,
    MIN_FREQ_TO_RENDER,
    OUTPUT_DIR,
    TESTING_MODE,
} from '../utils/envConsts';
import { ERROR, sanitizeName } from '../utils/utils';
import { createTopTuning } from '../tunings/createTopTuning';
import { newComplexTone, PulseProps } from '../gens/toneNextGen';
import { createEnvelope } from '../gens/createEnvelope';

const pathPrefix = OUTPUT_DIR;

export const generateSoundfont = (
    preset: SFG.Preset,

    sfzOffset?: number,
    sfzTopOffset?: number
) => {
    const { name: rawName, tuningParams } = preset;
    const tuning = createTopTuning(preset);
    const { baseFreq, partials, scale, scalaFormat } = tuning;
    const env = createEnvelope(preset.mainEnv, tuning, true);
    const loop = env.mode === 'SFZLOOP';

    const name = sanitizeName(rawName);
    const newPath = `${pathPrefix}/${name}`;
    if (!fs.existsSync(newPath)) {
        fs.mkdirSync(newPath);
    }
    writeScalaFile(newPath + '/scala', name, scalaFormat, tuningParams);

    /**najdi rozumny interval, vyrendrovane noty nech su v pocutelnom spektre */
    let removedFromBottom = 0;
    while (removedFromBottom < 128) {
        if (
            freqPlusCents(scale[removedFromBottom], baseFreq) >
            MIN_FREQ_TO_RENDER
        )
            break;
        removedFromBottom++;
    }

    let removedFromTop = 127;
    while (removedFromTop > 0) {
        if (freqPlusCents(scale[removedFromTop], baseFreq) < MAX_FREQ_TO_RENDER)
            break;
        removedFromTop--;
    }
    removedFromTop = 127 - removedFromTop;

    const finalOffset = sfzOffset ?? removedFromBottom;
    const finalCount = sfzTopOffset
        ? sfzTopOffset - finalOffset + 1
        : 128 - removedFromTop - removedFromBottom;
    // const finalCount2 =
    //     sfzOffset + notesToGenerateCount < 128 - removedFromTop
    //         ? notesToGenerateCount
    //         : 128 - removedFromTop - finalOffset;
    if (finalCount < 1) {
        ERROR(
            'Not enough notes to generate. Please edit preset settings or change sfz boundaries.'
        );
        process.exit(0);
    }
    if (TESTING_MODE) {
        console.log('removedFromTop ', removedFromTop);
        console.log('removedFromBottom ', removedFromBottom);
        console.log('sfzOffset ', sfzOffset);
        console.log('lowest note to generate ', finalOffset);
        console.log('highest note to generate ', finalOffset + finalCount - 1);
        console.log('finalCount ', finalCount);

        [...Array(finalCount).keys()].map((i) => {
            const freq = freqPlusCents(scale[i + finalOffset], baseFreq);
            console.log('Note: ', i + finalOffset, '. Frequency: ', freq);
        });
        console.log('lowest note to generate ', finalOffset);
        console.log('highest note to generate ', finalOffset + finalCount - 1);
        console.log('finalCount ', finalCount);

        process.exit(0);
    }
    /*  if (TESTING_MODE && userNumber('Render SFZ wavs? (87 = yes)\n') !== 87)
        return; */

    {
        /**----------------
         * RENDER WAV FILES
         * ----------------*/
    }

    const loopIndexes: { lStart: number; lEnd: number }[] = [];
    const scaleWaves = [...Array(finalCount).keys()].map((i) => {
        const freq = freqPlusCents(scale[i + finalOffset], baseFreq);

        process.stdout.write(
            `\r${i + 1}/${finalCount}: generating note ${
                i + finalOffset
            } with freq: ${freq}`
        );
        if (loop) {
            const {
                loopStartIndex,
                loopEndIndex,
                wave: loopedWave,
            } = newComplexTone(freq, partials, 20000, env);
            loopIndexes.push({ lStart: loopStartIndex, lEnd: loopEndIndex });
            return normalize(loopedWave);
        }

        const { wave } = newComplexTone(
            freq,
            partials,
            env.decay.maxDecayLength,
            env
        );
        return normalize(wave);
    });

    {
        /* WRITE ILUSTRATIVE WAVE FILE */
    }

    //console.log('Writing scale wave file to disc...');
    //const whole = normalize(connect(scaleW));
    //writeWaveFile(`${newPath}/${name}_scale.wav`, 1, SR, BR,whole);

    {
        /* WRITE SFZ */
    }
    console.log('\nWriting sfz soundfont to disc...\n');
    writeSfz(newPath, name, scaleWaves, finalOffset, {
        loop,
        releaseTime: env.release / 1000,
        loopIndexes,
    });
};
