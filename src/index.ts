import { SFG } from './types/endpoints';

import { readPreset, writePreset } from './io/presets';
import { generateSoundfont } from './sfz/generateSoundfont';
import {
    ALL_PRESETS,
    AMITY_PRESET,
    BR,
    DEFAULT_PRESET,
    MAX_ALLOWED_DECAY_TIME,
    OUTPUT_DIR,
    PRESET_MODE,
    SR,
    TESTING_MODE,
    TEST_PARAMS_ON,
} from './utils/envConsts';
import fs from 'fs';
import { normalize, softenEdges } from './gens/basicGenerators';
import { parseMidiEvents, renderMidiFile } from './midi/midiToWaves';
import { createTopTuning } from './tunings/createTopTuning';
import { writeWaveFile } from './wavefiles';
import { divideVector, genTopTuning, reduceToPeriod } from './tunings/topScale';
import { freqPlusCents, noteToFreq } from './midi/noteToFreq';
import { writeScalaFile } from './scala/writeScala';
import { dbToFactor, factorToDb, isInt } from './utils/utils';
import { newComplexTone } from './gens/toneNextGen';
import { createEnvelope } from './gens/createEnvelope';

var pjson = require('../package.json');

const ARGS = process.argv.slice(2);

const main = (args: string[]) => {
    var t0 = performance.now();

    if (args[0] === 'version') {
        console.log(`\nSFG version: ${pjson.version}\n`);
        console.log('Testing mode: ', TESTING_MODE ? 'yes' : 'no');
        process.exit(0);
    }
    if (TESTING_MODE) {
        console.log('Testing mode: ', TESTING_MODE ? 'yes' : 'no');
    }
    console.log('version:', pjson.version);

    /**************INIT*************** */
    if (args[0] === 'init') {
        ALL_PRESETS.forEach((p) => writePreset(p.name, p, PRESET_MODE));
        console.log(`\n${PRESET_MODE} presets initialized.\n`);
        //console.log(ALL_PRESETS);
        var t1 = performance.now();
        console.log(
            '\nDone in ' + Math.floor((t1 - t0) / 10) / 100 + ' seconds.\n'
        );
        process.exit(0);
    }
    /**************MIDI MODE*************** */
    if (args[0] === 'midi') {
        console.log(`MIDI MODE`);
        const presetName = args[1];
        const midiFileName = args[2];
        const stereoMode = args[3];
        let STEREO = false;
        if (presetName === undefined) {
            console.log('Preset not found!');
            process.exit(0);
        }
        const preset = readPreset(presetName, PRESET_MODE);

        if (midiFileName === undefined) {
            console.log('Source midi file not found!');
            process.exit(1);
        }

        if (stereoMode === 'stereo') {
            STEREO = true;
        }

        const output_wav =
            midiFileName[midiFileName.length - 1] === 'i'
                ? midiFileName.replaceAll('.midi', '_output_' + presetName)
                : midiFileName.replaceAll('.mid', '_output_' + presetName);

        renderMidiFile(
            preset,
            `${OUTPUT_DIR}/${midiFileName}`,
            output_wav,
            STEREO
        );

        var t1 = performance.now();
        console.log(
            '\nDone in ' + Math.floor((t1 - t0) / 10) / 100 + ' seconds.\n'
        );
        process.exit(0);
    }
    /**************SFZ MODE*************** */

    if (args[0] === 'sfz') {
        console.log(`SFZ Soundfont mode.`);
        const presetName = args[1];
        if (presetName === undefined) {
            console.log('Preset not found!');
            process.exit(0);
        }
        /**boundaries validation */
        let sfzOffset: number | undefined = parseInt(args[2]);
        if (isNaN(sfzOffset) || !isInt(sfzOffset)) {
            sfzOffset = undefined;
        } else {
            if (sfzOffset < 0) sfzOffset = 0;
        }
        let sfzTopOffset: number | undefined = parseInt(args[3]);
        if (isNaN(sfzTopOffset) || !isInt(sfzTopOffset)) {
            sfzTopOffset = undefined;
        } else {
            if (sfzTopOffset > 127) sfzTopOffset = 127;
        }
        /************* */
        const preset = readPreset(presetName, PRESET_MODE);
        generateSoundfont(preset, sfzOffset, sfzTopOffset); // todo parametrize

        var t1 = performance.now();
        console.log(
            '\nDone in ' + Math.floor((t1 - t0) / 10) / 100 + ' seconds.\n'
        );
        process.exit(0);
    }

    /**************SCALA MODE*************** */

    if (args[0] === 'scala') {
        console.log(`Scala mode.`);
        const presetName = args[1];
        if (presetName === undefined) {
            console.log('Preset not found!');
            process.exit(0);
        }
        const preset = readPreset(presetName, PRESET_MODE);

        const { tuningParams } = preset;

        const { scalaFormat } = genTopTuning(tuningParams);
        writeScalaFile(
            `${OUTPUT_DIR}`,
            `${preset.name}_scala`,
            scalaFormat,
            tuningParams
        );

        var t1 = performance.now();
        console.log(
            '\nDone in ' + Math.floor((t1 - t0) / 10) / 100 + ' seconds.\n'
        );
        process.exit(0);
    }
    /*************TEST NOTE MODE**************** */
    // sfg note 60 mavila adsr 5000
    if (args[0] === 'note') {
        //        console.log('Input format: ');
        //console.log('sfg note <midi note number> <preset> <sfz?>');
        let midiNote = parseInt(args[1]);
        const presetName = args[2];
        const TONE_MODE = args[3];
        const ADSR_LENGTH = args[4];

        let noteLength = parseInt(ADSR_LENGTH);
        if (!noteLength || noteLength < 10) noteLength = 5000;

        //load preset
        console.log(`Test one soundfont note mode.`);
        if (presetName === undefined) {
            console.log('Preset not found!');
            process.exit(0);
        }
        const preset = readPreset(presetName, PRESET_MODE);
        //parse midi
        if (midiNote === undefined || midiNote < 0 || midiNote > 127) {
            console.log('No valid midi note selected. Using note 60.');
            midiNote = 60;
        }
        const { name } = preset;

        const tuning = createTopTuning(preset);

        if (TONE_MODE === 'sfz') {
            console.log('Generating test midi sfz note ...');
            const env = createEnvelope(preset.mainEnv, tuning, true);
            const { baseFreq, partials, scale } = tuning;
            const freq = freqPlusCents(scale[midiNote], baseFreq);

            console.log('isSustained:', env.data.isSustained);
            noteLength = env.data.isSustained
                ? noteLength
                : env.attack + MAX_ALLOWED_DECAY_TIME;
            env.mode = env.data.isSustained ? 'SFZLOOP' : 'PERC';
            const { wave } = newComplexTone(freq, partials, noteLength, env);
            const normalized = normalize(wave);
            console.log('Writing test midi note to disc...');
            writeWaveFile(
                `${OUTPUT_DIR}/${name}_sfz_${midiNote}.wav`,
                //`${OUTPUT_DIR}/note_${midiNote}.wav`,
                1,
                SR,
                BR,
                normalize(normalized)
            );
            var t1 = performance.now();
            console.log(
                '\nDone in ' + Math.floor((t1 - t0) / 10) / 100 + ' seconds.\n'
            );

            return;
        }

        console.log('Generating test midi ADSR/PERC note ...');
        const env = createEnvelope(preset.mainEnv, tuning, false);
        const { baseFreq, partials, scale } = tuning;
        const freq = freqPlusCents(scale[midiNote], baseFreq);

        console.log('isSustained:', env.data.isSustained);

        //  noteLength = env.data.isSustained
        //      ? noteLength
        //      : env.attack + MAX_ALLOWED_DECAY_TIME;
        // console.log(noteLength);
        env.mode = env.data.isSustained ? 'ADSR' : 'PERC';
        const { wave } = newComplexTone(freq, partials, noteLength, env);
        const normalized = normalize(softenEdges(wave, 1, 10));
        console.log('Writing test midi note to disc...');
        writeWaveFile(
            `${OUTPUT_DIR}/${name}_${midiNote}.wav`,
            //`${OUTPUT_DIR}/note_${midiNote}.wav`,
            1,
            SR,
            BR,
            normalize(normalized)
        );
        var t1 = performance.now();
        console.log(
            '\nDone in ' + Math.floor((t1 - t0) / 10) / 100 + ' seconds.\n'
        );

        return;
    }

    /************************************************************************************************ */

    /***************************
     *                         *
     *       TEST PARAMS       *
     *                         *
     **************************/

    /************************************************************************************************ */

    /**************PARSE MIDI MODE*************** */
    if (args[0] === 'parsemidi' && TESTING_MODE) {
        console.log(`PARSE MIDI MODE`);
        const midiFileName = args[1];

        parseMidiEvents(`${OUTPUT_DIR}/${midiFileName}`);
        process.exit(0);
    }

    if (args[0] === 'cpp' && TESTING_MODE) {
        console.log(reduceToPeriod(2856, 1200));
        console.log(divideVector([1, 4, 4], 4));
    }

    if (args[0] === 'init_sfgp' && TEST_PARAMS_ON) {
        ALL_PRESETS.forEach((p) => writePreset(p.name, p, 'SFG'));
        console.log('\nSFG presets initialized.\n');
        return;
    }

    if (args[0] === 'readsfgp' && TEST_PARAMS_ON) {
        if (args[1] === undefined) {
            console.log('Preset not found!');
            return;
        }
        console.log(readPreset(args[1], 'SFG'));
        return;
    }

    if (args.length < -1 && TEST_PARAMS_ON) {
        console.log(`Manual input mode:`);
        const preset = DEFAULT_PRESET;
        const numerator = 256; // 648; //userNumber('Numerator: ');
        //const numerator = 648; //648; //userNumber('Numerator: ');
        const denominator = 243; //625; // userNumber('Denominator: ');
        //const denominator = 625; //625; // userNumber('Denominator: ');
        const scaleLength = 10; //userNumber('Tones in period: ');
        preset.name = 'MANUAL';
        preset.tuningParams.periodLength = scaleLength;
        preset.partialsSettings.attenPower = 1.2;
        // preset.tuningParams.negativeGenerators = 0;
        preset.tuningParams.commaToTemper = [numerator, denominator];

        generateSoundfont(preset);
        return;
    }

    if (args.length < -1 && TEST_PARAMS_ON) {
        // FIXME: temp -1
        let preset: SFG.Preset = {} as SFG.Preset;
        try {
            preset = readPreset('defaultPreset');
        } catch (error) {
            console.log('Default preset not found, creating one...');
            writePreset('defaultPreset', DEFAULT_PRESET);
            console.log(`Default preset was written do disc.`);
            preset = DEFAULT_PRESET;
        }
        preset = DEFAULT_PRESET;

        generateSoundfont(preset);
        return;
    }
    if (args[0] === 'reset' && TEST_PARAMS_ON) {
        //writePreset('defaultPreset', DEFAULT_PRESET, 'SFG');
        readPreset('defaultPreset', 'SFG');
        console.log(`Default preset was written do disc.`);
        return;
    }

    if (args[0] === 'viz' && TEST_PARAMS_ON) {
        console.log(`VIZ MODE`);

        const factorPerSec = dbToFactor(6);
        const susPointDb = 12;

        const SR = 44100;
        const oneStep = Math.pow(factorPerSec, 1 / SR);

        console.log(factorPerSec);
        const out = [];
        let dbVal = 0;
        let i = 0;
        while (dbVal < susPointDb || i > 99999999) {
            const factor = Math.pow(oneStep, i);
            dbVal = factorToDb(factor);
            i++;
            out.push(1 / factor);
            //out.push(dbVal);
        }

        const norm = normalize(out).slice(0, 10);
        console.log(norm);

        console.log(out.length / SR);

        fs.writeFileSync('./output.json', JSON.stringify({ data: norm }));

        return;
    }
    if (args[0] === 'top' && TEST_PARAMS_ON) {
        console.log(`TEST TOP TUNING MODE`);
        const presetName = args[1];
        if (presetName === undefined) {
            console.log('Preset not found!');
            process.exit(0);
        }
        const preset = readPreset(presetName, PRESET_MODE);
        //const preset = PYTH_PRESET;
        //PYTH_PRESET.tuningParams.commaToTemper[0] = 4;
        //PYTH_PRESET.tuningParams.commaToTemper[1] = 3;
        //preset.tuningParams.notTopTuning = true;
        const tunning = createTopTuning(preset);
        return;

        AMITY_PRESET.tuningParams.commaToTemper[0] = 5;
        AMITY_PRESET.tuningParams.commaToTemper[1] = 4;

        //const preset = readPreset(presetName, PRESET_MODE);

        //const tunning = createTopTuning(preset);

        return;
    }
    if (args[0] === 'topextra' && TEST_PARAMS_ON) {
        console.log(`TEST TOP TUNING MODE`);
        //let allSteps: Record<string, number[]> = {};
        //let allSteps = '0,1,2,3,4,5,6,7,8,9,10,11,12\n';
        let allSteps = 'Step/Iter,cis,d,dis,e,f,fis,g,gis,a,ais,h,c\n';
        for (let i = 0; i < 9; i++) {
            DEFAULT_PRESET.tuningParams.negativeGenerators = i;
            const tunning = createTopTuning(DEFAULT_PRESET);
            //tunning.steps?.join()
            let steps = i + ',';
            (tunning.steps ?? []).forEach((s) => (steps = steps + s + ','));
            allSteps = allSteps + steps + '\n';
        }
        fs.writeFileSync(`./binary/stepsPeriodInBUG.csv`, allSteps);

        return;
    }

    if (
        TESTING_MODE &&
        args.length === 2 &&
        args[0] === '-p' &&
        args[1] !== ''
    ) {
        const preset = readPreset(args[1]);
        console.log(`Preset ${preset.name} was loaded from ${args[1]}.json`);
        generateSoundfont(preset);
        return;
    }

    console.log('No valid parameters detected. Exiting.');
    return 0;
};

/***********  MAIN PROCESS START*************/
main(ARGS);
/***********  MAIN PROCESS START*************/

/**
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

{
    /*-----------------PERFORMANCE TEST---------------------*/
}
/*
const params: SFG.TuningParams = {
    commaToTemper: [81, 80],
    negativeGenerators: 5,
    periodLength: 12,
    centerNote: 60,
    centerNoteShift: 0,
};
const { primes, scale, scalaFormat } = genTopTuning(params);
const partials = getPartialsTable(primes, 150, 1.2);
console.log('Generating... \n');

var t0 = performance.now();
tunedWaveSpeedCheck({ amp: 1, freq: 50, ms: 65000 }, partials);
var t1 = performance.now();
console.log('Call to doSomething took ' + (t1 - t0) + ' milliseconds.');
console.log('Call to doSomething took ' + Math.floor((t1 - t0) / 10)/100 + ' seconds.');

*/
{
    /*-----------------PERFORMANCE TEST---------------------*/
}

//console.log('\n\nFinished. Have a nice day.');
